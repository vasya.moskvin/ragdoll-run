using System;
using TMPro;
using UnityEngine;

namespace GameEngine.Menu.Common
{
    public class CoinsPresenter : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text _text;

        public void Init(AccountModel accountModel)
        {
            _text.text = accountModel.Coins.ToString();
        }
    }
}
