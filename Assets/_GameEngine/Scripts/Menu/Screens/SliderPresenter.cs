using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GameEngine.Menu.Components
{
    public class SliderPresenter : MonoBehaviour
    {
        [SerializeField]
        private Image _slider;

        public virtual void UpdateSlider(float count, float maxCount)
        {
            _slider.DOFillAmount(count / maxCount, 0.5f);
        }

        protected void ChangeColor(Color color)
        {
            _slider.color = color;
        }
    }
}
