using System;
using UnityEngine;
using UnityEngine.UI;

public class CustomSlider : MonoBehaviour
{
    [SerializeField]
    private Slider _slider;

    [SerializeField]
    private Text _text;

    public event Action<int> Changed;

    public void Init(long value)
    {
        _text.text = $"{value}";
        _slider.SetValueWithoutNotify(value);
        _slider.onValueChanged.AddListener(
            sliderValue =>
            {
                _text.text = $"{(int) sliderValue}";
                Changed?.Invoke((int) sliderValue);
            });
    }
}
