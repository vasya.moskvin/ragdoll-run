using DG.Tweening;
using UnityEngine;

namespace GameEngine.Menu.Common
{
    public class HandMover : MonoBehaviour
    {
        [SerializeField]
        private Transform _hand;

        [SerializeField]
        private Transform _startPosition;

        [SerializeField]
        private Transform _endPosition;

        [SerializeField]
        private float _duration;

        private void Start()
        {
            _hand.position = _startPosition.position;
            _hand.DOMove(_endPosition.position, _duration).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        }
    }
}
