using System;
using System.Collections.Generic;
using GameEngine.Menu.Components;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameEngine.Menu.Common
{
    public class GameScreen : Screen
    {
        [SerializeField]
        private CustomButton _prefabButton;

        [SerializeField]
        private CustomButton _startDrill;

        [SerializeField]
        private Transform _parentButtons;

        [SerializeField]
        private VariableJoystick _variableJoystick;

        [SerializeField]
        private TMP_Text _levelText;

        [SerializeField, TextArea]
        private string _levelFormat;

        [SerializeField]
        private Tutorial _tutorial;

        [SerializeField]
        private SliderPresenter _progress;

        [SerializeField]
        private ArrowContainer _arrowContainer;

        private LevelModel _levelModel;
        private List<CustomButton> _customButtons = new List<CustomButton>();

        public override void SetArguments(ScreenArguments screenArguments)
        {
            base.SetArguments(screenArguments);

            _tutorial.gameObject.SetActive(screenArguments.Level.LevelSettings.IsTutorial);
            _levelText.text = string.Format(_levelFormat, Arguments.Level.LevelNumber);
            _levelModel = Arguments.Level;
            Arguments.Level.NoiseChanged += UpdateProgress;
            Arguments.Level.GoalFinished += OnGoalFinish;
            if (Arguments.TutorialModel != null)
            {
                TutorialModelOnChanged(Arguments.TutorialModel);
                Arguments.TutorialModel.Changed += TutorialModelOnChanged;
            }

            UpdateProgress();
            InitButtons();
        }

        private void TutorialModelOnChanged(TutorialModel sender)
        {
            _tutorial.PressDown.SetActive(sender.IsNeedPressDown);
            _tutorial.PressUp.SetActive(sender.IsNeedPressUp);
            _tutorial.Text.text = sender.TutorialText;
        }

        private void UpdateProgress()
        {
            _progress.UpdateSlider(Arguments.Level.Noise, Arguments.Level.MaxNoise);
        }

        private void ClearButtons()
        {
            foreach (var button in _customButtons)
            {
                Destroy(button.gameObject);
            }
            _customButtons.Clear();
        }

        private void InitButtons()
        {
            ClearButtons();
            foreach (var setting in Arguments.Level.LevelSettings.BitSettings)
            {
                var button = Instantiate(_prefabButton, _parentButtons);
                _customButtons.Add(button);
                button.Init(setting.Id, () => Arguments.Level.SetBit(setting));
            }

            _startDrill.Init("Start", () =>
            {
                _startDrill.gameObject.SetActive(false);
                _parentButtons.gameObject.SetActive(false);
                Arguments.Level.StartDrill();
            });
        }

        private void ToMenu()
        {
            Arguments.Game.CancelLevel();
        }

        private void OnGoalFinish()
        {
            _parentButtons.gameObject.SetActive(true);
        }

        private void OnDisable()
        {
            if (Arguments?.Level != null)
            {
                Arguments.Level.NoiseChanged -= UpdateProgress;
            }
        }

        private void OnEnable()
        {
            _variableJoystick.OnPointerUp(new PointerEventData(EventSystem.current));
        }
    }
}