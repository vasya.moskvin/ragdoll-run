using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameEngine.Menu.Components
{
    public class LevelPresenter : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text _text;

        [SerializeField]
        private Image _image;

        [SerializeField]
        private Sprite _successLevel;

        [SerializeField]
        private Sprite _nextLevel;

        [SerializeField]
        private Sprite _baseSprite;

        [SerializeField]
        private Color _successColorText;

        [SerializeField]
        private Color _nextColorText;

        [SerializeField]
        private Color _baseColorText;

        public void Init(AccountModel accountModel, int indexLevel)
        {
            _text.text = (indexLevel + 1).ToString();
            if (indexLevel == accountModel.NextLevelIndex)
            {
                _image.sprite = _nextLevel;
                _text.color = _nextColorText;
                return;
            }

            if (indexLevel < accountModel.NextLevelIndex)
            {
                _image.sprite = _successLevel;
                _text.color = _successColorText;
                return;
            }

            _image.sprite = _baseSprite;
            _text.color = _baseColorText;
        }
    }
}
