using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace GameEngine.Menu.Components
{
    public class Arrow : MonoBehaviour
    {
        [SerializeField]
        private List<Image> _images;

        [SerializeField]
        private GameObject _direct;

        private Camera _camera;
        private Unit _target;

        public void Init(Unit target,
                         Camera mainCamera,
                         Action<Arrow> freeCallback)
        {
            _camera = mainCamera;
            _target = target;
        }

        public void CheckActive()
        {
            if (_target == null)
            {
                gameObject.SetActive(false);
                return;
            }
            SetColor(_target.GetColorProgress(), _target.GetProgress());
            var screenPointStart = _camera.WorldToScreenPoint(_target.transform.position);

            if (IsVisiblePoint(_target.transform.position))
            {
                transform.position = screenPointStart;
                gameObject.SetActive(false);
                return;
            }

            var screenPointFinish = _center;
            screenPointStart.z = 0;
            var typeByPoint = GetTypeByPoint(screenPointStart);

            if (typeByPoint == ArrowType.None)
            {
                gameObject.SetActive(false);
                return;
            }
            gameObject.SetActive(_target.gameObject.activeInHierarchy && _target.IsActive);

            var result = GetPositionOne(
                typeByPoint,
                screenPointStart,
                screenPointFinish);

            // Debug.DrawLine(screenPointStart, screenPointFinish);
            result.z = 0;

            transform.rotation = Quaternion.identity;
            transform.position = result;
            transform.right = result - screenPointStart;
        }

        private void SetColor(Color color, float progress)
        {
            foreach (var image in _images)
            {
                image.color = color;
                image.fillAmount = 1 - progress;
            }
        }

        private class Border
        {
            public Vector3 One { get; set; }
            public Vector3 Two { get; set; }

            public bool IsContains(Vector3 point, Vector3 center)
            {
                var anglePoint = Mathf.Atan2(point.x - center.x, point.y - center.y) * Mathf.Rad2Deg;
                var angleOne = Mathf.Atan2(One.x - center.x, One.y - center.y) * Mathf.Rad2Deg;
                var angleTwo = Mathf.Atan2(Two.x - center.x, Two.y - center.y) * Mathf.Rad2Deg;
                return angleOne < anglePoint && anglePoint < angleTwo;
            }
        }

        private readonly Border _bottom = new Border { One = new Vector3(Screen.width, 0), Two = new Vector3(0, 0) };
        private readonly Border _left = new Border { One = new Vector3(0, 0), Two = new Vector3(0, Screen.height) };
        private readonly Border _top = new Border { One = new Vector3(0, Screen.height), Two = new Vector3(Screen.width, Screen.height) };
        private readonly Border _right = new Border { One = new Vector3(Screen.width, Screen.height), Two = new Vector3(Screen.width, 0) };
        private readonly Vector3 _center = new Vector3(Screen.width / 2f, Screen.height / 2f);

        private Vector3 GetPositionOne(ArrowType type,
                                       Vector3 screenPointStart,
                                       Vector3 screenPointFinish)
        {
            Vector3 position;
            switch (type)
            {
                case ArrowType.Top:
                    position = Utils.Math.Intersection(
                        screenPointStart,
                        screenPointFinish,
                        _top.One,
                        _top.Two);
                    position.z = 0;
                    break;
                case ArrowType.Bottom:
                    position = Utils.Math.Intersection(
                        screenPointStart,
                        screenPointFinish,
                        _bottom.One,
                        _bottom.Two);
                    position.z = 0;
                    break;
                case ArrowType.Left:
                    position = Utils.Math.Intersection(
                        screenPointStart,
                        screenPointFinish,
                        _left.One,
                        _left.Two);
                    position.z = 0;
                    break;
                case ArrowType.Right:
                    position = Utils.Math.Intersection(
                        screenPointStart,
                        screenPointFinish,
                        _right.One,
                        _right.Two);
                    position.z = 0;
                    break;
                case ArrowType.None:
                    position = _center;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
            return position;
        }

        private ArrowType GetTypeByPoint(Vector3 point)
        {
            if (_top.IsContains(point, _center))
            {
                return ArrowType.Top;
            }

            if (_left.IsContains(point, _center))
            {
                return ArrowType.Left;
            }

            if (_right.IsContains(point, _center))
            {
                return ArrowType.Right;
            }

            return ArrowType.Bottom;
        }

        private bool IsVisiblePoint(Vector3 point)
        {
            point = _camera.WorldToScreenPoint(point);
            return point.x >= 0 && point.x <= Screen.width &&
                   point.y >= 0 && point.y <= Screen.height;
        }
    }

    public enum ArrowType
    {
        None,
        Top,
        Bottom,
        Left,
        Right
    }
}

