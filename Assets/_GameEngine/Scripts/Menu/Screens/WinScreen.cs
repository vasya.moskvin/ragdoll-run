using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameEngine.Menu.Common
{
    public class WinScreen : Screen
    {
        [SerializeField]
        private Button _next;

        [SerializeField]
        private TMP_Text _level;

        [SerializeField]
        private CoinsPresenter _coinsPresenter;

        [SerializeField]
        private TMP_Text _newCoinsPresenter;

        private void Start()
        {
            _next.onClick.AddListener(StartGame);
        }

        private void OnEnable()
        {
            _level.text = $"LEVEL {AccountData.GetAccountModel().NextLevelIndex}";
            _coinsPresenter.Init(AccountData.GetAccountModel());
            if (Arguments != null)
            {
                _newCoinsPresenter.text = $"+{Arguments.RewardCoins}";
            }
        }

        private void StartGame()
        {
            if (Arguments?.Game != null)
            {
                Arguments.Game.StartLevel();
            }
        }
    }
}
