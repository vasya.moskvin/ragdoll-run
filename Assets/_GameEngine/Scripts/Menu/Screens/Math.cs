using UnityEngine;

namespace GameEngine.Utils
{
    public static class Math
    {
        public static Vector3 Intersection(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
        {
            float xo = a.x, yo = a.y, zo = a.z;
            float p = b.x - a.x, q = b.y - a.y, r = b.z - a.z;

            float x1 = c.x, y1 = c.y, z1 = c.z;
            float p1 = d.x - c.x, q1 = d.y - c.y, r1 = d.z - c.z;

            var x = (xo * q * p1 - x1 * q1 * p - yo * p * p1 + y1 * p * p1) /
                    (q * p1 - q1 * p);
            var y = (yo * p * q1 - y1 * p1 * q - xo * q * q1 + x1 * q * q1) /
                    (p * q1 - p1 * q);
            var z = (zo * q * r1 - z1 * q1 * r - yo * r * r1 + y1 * r * r1) /
                    (q * r1 - q1 * r);

            return new Vector3(x, y, z);
        }
    }
}
