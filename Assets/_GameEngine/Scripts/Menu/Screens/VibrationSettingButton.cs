using UnityEngine;
using UnityEngine.UI;

namespace GameEngine.Menu.Common
{
    public class VibrationSettingButton : MonoBehaviour
    {
        [SerializeField]
        private Button _button;
        [SerializeField]
        private Image _targetImage;
        [SerializeField]
        private Sprite _off;
        [SerializeField]
        private Sprite _on;

        private void Start()
        {
            SetStatus();
            _button.onClick.AddListener(ChangeState);
        }

        private void ChangeState()
        {
            AccountData.GetAccountModel().IsEnableVibration = !AccountData.GetAccountModel().IsEnableVibration;

            SetStatus();

            VibrationMediator.Vibrate(GlobalSettings.ChangeVibration);
        }

        private void SetStatus()
        {
            _targetImage.sprite = AccountData.GetAccountModel().IsEnableVibration
                                      ? _on
                                      : _off;
        }
    }
}
