using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CustomButton : MonoBehaviour
{
    [SerializeField]
    private Button _button;

    [SerializeField]
    private TMP_Text _title;

    private Action _callback;

    public void Init(string title, Action callback)
    {
        gameObject.SetActive(true);
        _button.gameObject.SetActive(true);
        _title.text = title;
        _callback = callback;
    }

    private void Start()
    {
        _button.onClick.AddListener(Click);
    }

    private void Click()
    {
        _callback?.Invoke();
    }

    private void OnDestroy()
    {
        _button.onClick.RemoveListener(Click);
    }
}
