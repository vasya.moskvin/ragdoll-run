using UnityEngine;
using UnityEngine.UI;

namespace GameEngine.Menu.Common
{
    public class LoseScreen : Screen
    {
        [SerializeField]
        private Button _repeat;

        private void Start()
        {
            _repeat.onClick.AddListener(StartGame);
        }

        private void StartGame()
        {
            if (Arguments?.Game != null)
            {
                Arguments.Game.StartLevel();
            }
        }
    }
}
