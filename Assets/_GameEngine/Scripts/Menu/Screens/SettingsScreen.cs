using UnityEngine;
using UnityEngine.UI;

namespace GameEngine.Menu.Common
{
    public class SettingsScreen : Screen
    {
        [SerializeField]
        private CustomSlider _ringAmplitude;
        [SerializeField]
        private InputField _ringDuration;
        [SerializeField]
        private Button _testRingButton;

        [SerializeField]
        private CustomSlider _enemyAmplitude;
        [SerializeField]
        private InputField _enemyDuration;
        [SerializeField]
        private Button _testEnemyButton;

        public void Start()
        {
            
        }
    }
}