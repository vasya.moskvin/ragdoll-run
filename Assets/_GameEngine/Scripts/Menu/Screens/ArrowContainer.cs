using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameEngine.Menu.Components
{
    public class ArrowContainer : MonoBehaviour
    {
        [SerializeField]
        private Arrow _arrow;

        private List<Arrow> _arrows = new List<Arrow>();
        private Coroutine _coroutine;

        private void Start()
        {
            _arrow.gameObject.SetActive(false);
        }

        public void SetObject(List<Unit> objects, Camera screenCamera)
        {
            foreach (var arrow in _arrows)
            {
                Destroy(arrow.gameObject);
            }
            _arrows.Clear();
            foreach (var unit in objects)
            {
                var arrow = Instantiate(_arrow, transform);
                arrow.Init(unit, screenCamera, null);
                _arrows.Add(arrow);
            }
            _arrow.gameObject.SetActive(false);
            DisableCheck();

            Invoke(nameof(DelayLaunch), 0.1f);
        }

        private void DelayLaunch()
        {
            _coroutine = StartCoroutine(CheckVisibleTarget());
        }

        private void DisableCheck()
        {
            if (_coroutine != null)
            {
                StopCoroutine(_coroutine);
                _coroutine = null;
            }
        }

        private IEnumerator CheckVisibleTarget()
        {
            while (true)
            {
                yield return new WaitForSeconds(0.1f);

                foreach (var arrow in _arrows)
                {
                    arrow.CheckActive();
                }
            }
        }

        private void OnDisable()
        {
            DisableCheck();
        }
    }
}
