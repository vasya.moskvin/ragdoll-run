using System.Collections.Generic;
using UnityEngine;

namespace GameEngine.Menu.Components
{
    public class ProgressPresenter : MonoBehaviour
    {
        [SerializeField]
        private List<LevelPresenter> _levelPresenters;

        public void Init(AccountModel accountModel)
        {
            var middleTextIndex = _levelPresenters.Count / 2;
            if (accountModel.NextLevelIndex >= middleTextIndex)
            {
                for (var i = 0; i < _levelPresenters.Count; i++)
                {
                    _levelPresenters[i].Init(accountModel, accountModel.NextLevelIndex - middleTextIndex + i);
                }
            }
            else
            {
                for (var i = 0; i < _levelPresenters.Count; i++)
                {
                    _levelPresenters[i].Init(accountModel, i);
                }
            }
        }
    }
}
