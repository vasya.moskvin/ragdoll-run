using UnityEngine;

namespace GameEngine.Menu.Components
{
    public class SliderGradientPresenter : SliderPresenter
    {
        [SerializeField]
        private Gradient _gradient;
        public override void UpdateSlider(float count, float maxCount)
        {
            base.UpdateSlider(count, maxCount);

            ChangeColor(_gradient.Evaluate(count / maxCount));
        }
    }
}
