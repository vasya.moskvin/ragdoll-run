using GameEngine.Menu.Components;
using UnityEngine;
using UnityEngine.UI;

namespace GameEngine.Menu.Common
{
    public class MainScreen : Screen
    {
        [SerializeField]
        private Button _start;

        [SerializeField]
        private Button _settings;

        [SerializeField]
        private CoinsPresenter _coinsPresenter;

        [SerializeField]
        private ProgressPresenter _progressPresenter;

        private void Start()
        {
            _start.onClick.AddListener(StartGame);
            _settings.onClick.AddListener(OpenSettings);
        }

        private void OnEnable()
        {
            _coinsPresenter.Init(AccountData.GetAccountModel());
            _progressPresenter.Init(AccountData.GetAccountModel());
        }

        private void OpenSettings()
        {
            Show(ScreenType.Settings, true, ScreenArguments.Empty());
        }

        private void StartGame()
        {
            if (Arguments?.Game != null)
            {
                Arguments.Game.StartLevel();
            }
        }
    }
}
