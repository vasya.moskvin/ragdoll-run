using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GameEngine.Menu.Components
{
    public class Marker : MonoBehaviour
    {
        private const float OffsetValue = 50;

        [SerializeField]
        private Image _pointOne;

        [SerializeField]
        private Image _pointTwo;

        private Camera _camera;

        public void Init(GameObject laserStart,
                         GameObject laserStop,
                         Camera mainCamera,
                         TypeAttention typeAttention,
                         float duration)
        {
            _camera = mainCamera;

            var screenPointStart = _camera.WorldToScreenPoint(laserStart.transform.position);
            var screenPointFinish = _camera.WorldToScreenPoint(laserStop.transform.position);

            screenPointStart.z = 1;
            screenPointFinish.z = 1;
            var positionOne = GetPositionOne(typeAttention, screenPointStart, screenPointFinish);
            var positionTwo = GetPositionTwo(typeAttention, screenPointStart, screenPointFinish);

            positionOne.z = 1;
            positionTwo.z = 1;

            _pointOne.transform.position = positionOne;
            _pointTwo.transform.position = positionTwo;

            _pointOne.DOFade(0.4f, 0.2f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
            _pointTwo.DOFade(0.4f, 0.2f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);

            gameObject.SetActive(true);
            Destroy(gameObject, duration);
        }

        private Vector3 GetPositionOne(TypeAttention type,
                                      Vector3 screenPointStart,
                                      Vector3 screenPointFinish)
        {
            Vector3 positionOne;
            if (type == TypeAttention.Vertical)
            {
                positionOne = Utils.Math.Intersection(
                    screenPointStart,
                    screenPointFinish,
                    new Vector3(0, 0),
                    new Vector3(Screen.width, 0));
                positionOne.y += OffsetValue;
                return positionOne;
            }

            positionOne = Utils.Math.Intersection(
                screenPointStart,
                screenPointFinish,
                new Vector3(0, 0),
                new Vector3(0, Screen.height));
            positionOne.x += OffsetValue;
            return positionOne;
        }

        private Vector3 GetPositionTwo(TypeAttention type,
                                       Vector3 screenPointStart,
                                       Vector3 screenPointFinish)
        {
            Vector3 positionTwo;
            if (type == TypeAttention.Vertical)
            {
                positionTwo = Utils.Math.Intersection(
                    screenPointStart,
                    screenPointFinish,
                    new Vector3(0, Screen.height),
                    new Vector3(Screen.width, Screen.height));
                positionTwo.y -= OffsetValue;
                return positionTwo;
            }

            positionTwo = Utils.Math.Intersection(
                screenPointStart,
                screenPointFinish,
                new Vector3(Screen.width, 0),
                new Vector3(Screen.width, Screen.height));
            positionTwo.x -= OffsetValue;
            return positionTwo;
        }

        // public void Disable()
        // {
        //     gameObject.SetActive(false);
        // }

        // private bool IsVisiblePoint(Vector3 point)
        // {
        //     point = _camera.WorldToScreenPoint(point);
        //     return point.x >= 0 && point.x <= Screen.width &&
        //            point.y >= 0 && point.y <= Screen.height;
        // }
    }
}

public enum TypeAttention
{
    Horizontal,
    Vertical
}
