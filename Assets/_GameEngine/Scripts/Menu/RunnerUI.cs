using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RunnerUI : MonoBehaviour
{
    [SerializeField]
    private TMP_Text _speed;

    [SerializeField]
    private Button _restart;

    [SerializeField]
    private Player _player;

    private Color _baseColor;
    private TweenerCore<Color, Color, ColorOptions> _doColor;
    private void Start()
    {
        _baseColor = _speed.color;
        _restart.onClick.AddListener(() => { SceneManager.LoadScene(0); });
        _player.BoostAdded += OnBoostAdded;
    }

    private void OnBoostAdded(Boost boost)
    {
        AddBoost(boost);
    }

    private void Update()
    {
        _speed.text = $"Speed: {_player.Speed:F2}";
    }

    private void AddBoost(Boost boost)
    {
        var color = boost.SpeedEffect > 0
                        ? Color.green
                        : Color.red;
        if (_doColor != null)
        {
            _doColor.Kill();
        }

        _doColor = _speed.DOColor(color, 0.3f).SetLoops(4, LoopType.Restart).OnComplete(() => _speed.color = _baseColor);
    }
}
