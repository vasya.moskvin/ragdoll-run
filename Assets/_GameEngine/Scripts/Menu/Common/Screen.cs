using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameEngine.Menu.Common
{
    public class Screen : MonoBehaviour
    {
        [SerializeField]
        private List<Button> _backButtons = new List<Button>();

        protected ScreenArguments Arguments;
        public Menu CurrentMenu { get; private set; }

        public virtual void SetCurrentMenu(Menu menu)
        {
            CurrentMenu = menu;
        }

        protected void Awake()
        {
            foreach (var back in _backButtons)
            {
                back.onClick.AddListener(Back);
            }
        }

        public void Show(ScreenType screenType,
                         bool needAddingHistory = true,
                         ScreenArguments screenArguments = null)
        {
            CurrentMenu.Show(screenType, needAddingHistory, screenArguments);
        }

        public virtual void SetArguments(ScreenArguments screenArguments)
        {
            Arguments = screenArguments;
        }

        protected void Back()
        {
            CurrentMenu.ShowPrevious();
        }
    }
}