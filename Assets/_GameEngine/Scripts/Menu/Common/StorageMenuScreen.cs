using System;
using UnityEngine;

namespace GameEngine.Menu.Common
{
    [Serializable]
    public class StorageMenuScreen
    {
        [SerializeField]
        private ScreenType _screenType;

        [SerializeField]
        private GameObject _screenGameObject;

        public ScreenType ScreenType => _screenType;

        public GameObject ScreenGameObject => _screenGameObject;

        private Screen _screen;

        public void SetArguments(ScreenArguments screenArguments)
        {
            if (_screen == null)
            {
                _screen = ScreenGameObject.GetComponent<Screen>();
            }

            if (_screen != null)
            {
                _screen.SetArguments(screenArguments);
            }
        }
    }
}