namespace GameEngine.Menu.Common
{
    public enum ScreenType
    {
        None = 0,
        Main = 1,
        Game = 2,
        Win = 3,
        Lose = 4,
        Settings = 5,
    }
}