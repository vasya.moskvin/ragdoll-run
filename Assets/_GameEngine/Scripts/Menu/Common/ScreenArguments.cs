using System.Collections.Generic;
using UnityEngine;

namespace GameEngine.Menu.Common
{
    public class ScreenArguments
    {
        public LevelModel Level { get; set; }
        public Game Game { get; set; }
        public int RewardCoins { get; set; }
        public Camera Camera { get; set; }

        public TutorialModel TutorialModel { get; set; }
        // public List<Hole> Goals { get; set; }

        public static ScreenArguments Empty()
        {
            return new ScreenArguments();
        }
    }
}