namespace GameEngine.Menu.Common
{
    public class HistoryScreenElement
    {
        public ScreenType Type { get; set; } = ScreenType.None;
        public ScreenArguments Arguments { get; set; } = ScreenArguments.Empty();
    }
}
