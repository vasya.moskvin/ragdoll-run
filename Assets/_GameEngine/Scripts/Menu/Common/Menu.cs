using System.Collections.Generic;
using UnityEngine;

namespace GameEngine.Menu.Common
{
    public class Menu : MonoBehaviour
    {
        [SerializeField]
        private List<StorageMenuScreen> _storage = new List<StorageMenuScreen>();

        private readonly Stack<HistoryScreenElement> _historyScreen = new Stack<HistoryScreenElement>();
        private HistoryScreenElement _currentScreen = new HistoryScreenElement();

        private void Start()
        {
            foreach (var element in _storage)
            {
                var menuLink = element.ScreenGameObject.GetComponent<Screen>();
                if (menuLink != null)
                {
                    menuLink.SetCurrentMenu(this);
                }
            }
        }

        private void HideAll()
        {
            foreach (var element in _storage)
            {
                element.ScreenGameObject.SetActive(false);
            }
        }

        public void Show(ScreenType screenType,
                         bool needAddingHistory = true,
                         ScreenArguments screenArguments = null)
        {
            var screen = _storage.Find(element => element.ScreenType == screenType);
            if (screen == null)
            {
                return;
            }

            if (screenType == _currentScreen.Type)
            {
                return;
            }

            HideAll();
            if (screenArguments == null)
            {
                screenArguments = ScreenArguments.Empty();
            }

            if (needAddingHistory)
            {
                _historyScreen.Push(_currentScreen);
            }

            screen.SetArguments(screenArguments);
            screen.ScreenGameObject.SetActive(true);

            _currentScreen = new HistoryScreenElement { Type = screenType, Arguments = screenArguments };
        }

        public void ShowPrevious()
        {
            if (_historyScreen.Count > 0)
            {
                var screenElement = _historyScreen.Pop();
                Show(screenElement.Type, false, screenElement.Arguments);
            }
        }
    }
}