using System;
using UnityEngine;

public class RunnerJoystick : MonoBehaviour
{
    [SerializeField]
    private VariableJoystick _variableJoystick;

    [SerializeField]
    private Player _player;

    private void Update()
    {
        if (Mathf.Abs(_variableJoystick.Horizontal) > 0.01f)
        {
            _player.MoveHorizontal(_variableJoystick.Horizontal);
        }
    }
}