using System;
using UnityEngine;

[Serializable]
public class Boost
{
    [SerializeField]
    private float _speedEffect;

    [SerializeField]
    private Material _material;

    public float SpeedEffect => _speedEffect;

    public Material Material => _material;
}
