using UnityEngine;

public class TestBomb : MonoBehaviour
{
    [SerializeField]
    private Transform _boomTransform;

    public void Boom()
    {
        var radius = 3;
        var explosionForce = 70;
        var colliders = Physics.OverlapSphere(_boomTransform.position, radius);

        foreach (var col in colliders)
        {
            var rb = col.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(explosionForce, _boomTransform.position, radius);
            }

            col.isTrigger = true;
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.3f);
        Gizmos.DrawSphere(_boomTransform.position, .3f);
    }
#endif
}
