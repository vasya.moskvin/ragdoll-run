using System;
using DG.Tweening;
using UnityEngine;

public class Lifebuoy : MonoBehaviour
{
    [SerializeField]
    private Collider _collider;

    private const float PauseTime = 30;
    private float _pauseTime;

    public void SetActiveTrigger(bool isActive)
    {
        _collider.enabled = isActive;
    }

    private void OnTriggerEnter(Collider other)
    {
        var unit = other.gameObject.GetComponent<Unit>();
        if(unit != null)
        {
            transform.position = new Vector3(unit.transform.position.x, transform.position.y, unit.transform.position.z);
            _pauseTime = 0;
            unit.PauseTimer();

            DOTween.To(GetTime, SetTime, PauseTime, PauseTime)
                   .SetEase(Ease.Linear)
                   .OnComplete(() =>
                   {
                       unit.PlayTimer();
                       Destroy(gameObject);
                   });
        }
    }

    private void SetTime(float time)
    {
        _pauseTime = time;
    }

    private float GetTime()
    {
        return _pauseTime;
    }
}