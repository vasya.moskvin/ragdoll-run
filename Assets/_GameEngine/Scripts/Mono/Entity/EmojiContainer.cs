#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmojiContainer : MonoBehaviour
{
    [SerializeField]
    private float _radius;

    [SerializeField]
    private float _emojiRadius = 0.4f;

    [SerializeField]
    private int _countEmoji;

    [SerializeField]
    private List<Emoji> _emojisPrefabs;

    [SerializeField]
    private bool _isAvailableRepeatingEmoji;

    private readonly List<Emoji> _emojis = new List<Emoji>();
    private readonly List<int> _usingIndexes = new List<int>();

    public void Play()
    {
        StartCoroutine(PlayCoroutine());
    }

    private IEnumerator PlayCoroutine()
    {
        foreach (var emoji in _emojis)
        {
            Destroy(emoji.gameObject);
        }
        _emojis.Clear();
        _usingIndexes.Clear();

        for (var index = 0; index < _countEmoji; index++)
        {
            var angle = index * (2 * Mathf.PI / _countEmoji);
            var position = new Vector3(
                Mathf.Cos(angle) * _radius,
                Mathf.Sin(angle) * _radius,
                0);
            var emoji = Instantiate(GetRandomEmoji(), transform);
            var randomPosition = Random.insideUnitCircle * _emojiRadius;
            emoji.transform.position = position + transform.position + new Vector3(randomPosition.x, randomPosition.y);
            emoji.Play();
            emoji.gameObject.SetActive(true);
            _emojis.Add(emoji);
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForEndOfFrame();
    }

    private Emoji GetRandomEmoji()
    {
        int index;
        bool successSelect;
        do
        {
            successSelect = false;
            index = Random.Range(0, _emojisPrefabs.Count);
            if (_isAvailableRepeatingEmoji)
            {
                successSelect = true;
            }
            else
            {
                if (!_usingIndexes.Contains(index))
                {
                    successSelect = true;
                }
            }

            if (_emojisPrefabs.Count < _countEmoji)
            {
                successSelect = true;
            }
        }
        while (!successSelect);
        _usingIndexes.Add(index);
        return _emojisPrefabs[index];
    }

    private void OnDrawGizmosSelected()
    {
#if UNITY_EDITOR
        Handles.DrawWireDisc(transform.position, Vector3.forward, _radius);
#endif
    }
}