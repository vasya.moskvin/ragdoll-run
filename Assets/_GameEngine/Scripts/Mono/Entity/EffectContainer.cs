using UnityEngine;

public class EffectContainer : MonoBehaviour
{
    [SerializeField]
    private EmojiContainer _emojiContainer;

    [SerializeField]
    private bool _isPlayInOnEnable;

    private void OnEnable()
    {
        if (_isPlayInOnEnable)
        {
            Play();
        }
    }

    public void Play()
    {
        _emojiContainer.Play();
    }
}