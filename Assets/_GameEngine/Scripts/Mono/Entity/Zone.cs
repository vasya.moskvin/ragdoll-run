using System.Collections.Generic;
using UnityEngine;

public class Zone : MonoBehaviour
{
    [SerializeField]
    private Transform _top;

    [SerializeField]
    private Transform _bottom;

    [SerializeField]
    private Transform _left;

    [SerializeField]
    private Transform _right;

    [SerializeField]
    private List<Zone> _voidZones;

    public bool IsContains(Vector3 point)
    {
        if (point.x < _left.position.x)
        {
            return false;
        }

        if (point.x > _right.position.x)
        {
            return false;
        }

        if (point.z < _bottom.position.z)
        {
            return false;
        }

        if (point.z > _top.position.z)
        {
            return false;
        }

        foreach (var voidZone in _voidZones)
        {
            if (voidZone.IsContains(point))
            {
                return false;
            }
        }

        return true;
    }
}