using UnityEngine;

public class HandThief : MonoBehaviour
{
    [SerializeField]
    private Animator _animator;

    private int Idle = Animator.StringToHash("Idle");
    private int Point = Animator.StringToHash("Point");
    private int GrabLarge = Animator.StringToHash("GrabLarge");
    private int GrabSmall = Animator.StringToHash("GrabSmall");
    private int GrabStickUp = Animator.StringToHash("GrabStickUp");
    private int GrabStickFront = Animator.StringToHash("GrabStickFront");
    private int ThumbUp = Animator.StringToHash("ThumbUp");
    private int Fist = Animator.StringToHash("Fist");
    private int Gun = Animator.StringToHash("Gun");
    private int GunShoot = Animator.StringToHash("GunShoot");
    private int PushButton = Animator.StringToHash("PushButton");
    private int Spread = Animator.StringToHash("Spread");
    private int MiddleFinger = Animator.StringToHash("MiddleFinger");
    private int Peace = Animator.StringToHash("Peace");
    private int OK = Animator.StringToHash("OK");
    private int Phone = Animator.StringToHash("Phone");
    private int Rock = Animator.StringToHash("Rock");
    private int Natural = Animator.StringToHash("Natural");
    private int Number3 = Animator.StringToHash("Number3");
    private int Number4 = Animator.StringToHash("Number4");
    private int Number3V2 = Animator.StringToHash("Number3V2");
    private int HoldViveController = Animator.StringToHash("HoldViveController");
    private int PressTriggerViveController = Animator.StringToHash("PressTriggerViveController");
    private int HoldOculusController = Animator.StringToHash("HoldOculusController");
    private int PressTriggerOculusController = Animator.StringToHash("PressTriggerOculusController");

    private void Start()
    {
        _animator.SetTrigger(GrabLarge);
    }
}
