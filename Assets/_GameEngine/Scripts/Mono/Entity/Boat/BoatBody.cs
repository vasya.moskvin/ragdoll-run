using UnityEngine;

public class BoatBody : MonoBehaviour
{
    [SerializeField]
    private Collider _collider;

    public void SetActiveTrigger(bool isActive)
    {
        _collider.enabled = isActive;
    }

    private void OnTriggerEnter(Collider other)
    {
        var unit = other.gameObject.GetComponent<Unit>();
        if(unit != null)
        {
            // unit.TryDie(GlobalSettings.ChangeVibration);
        }
    }
}
