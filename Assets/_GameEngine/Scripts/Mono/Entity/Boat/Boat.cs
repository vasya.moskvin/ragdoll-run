using DG.Tweening;
using UnityEngine;

public class Boat : MonoBehaviour
{
    [SerializeField]
    private BoatBody _body;

    [SerializeField]
    private BoatSaveZone _saveZone;

    public void ActivateFall()
    {
        _body.SetActiveTrigger(true);
        _saveZone.SetActiveTrigger(false);
    }

    public void ActivateSaveZone()
    {
        _body.SetActiveTrigger(false);
        _saveZone.SetActiveTrigger(true);
        Move();
    }

    private void Move()
    {
        transform.DOMove(transform.position + 100 * transform.forward, 10).SetDelay(2f).OnPlay(
            () =>
            {
                _body.SetActiveTrigger(false);
                _saveZone.SetActiveTrigger(false);
            });
    }
}