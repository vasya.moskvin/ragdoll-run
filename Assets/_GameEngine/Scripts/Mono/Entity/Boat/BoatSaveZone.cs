using DG.Tweening;
using UnityEngine;

public class BoatSaveZone : MonoBehaviour
{
    [SerializeField]
    private Collider _collider;

    public void SetActiveTrigger(bool isActive)
    {
        _collider.enabled = isActive;
    }

    private void OnTriggerEnter(Collider other)
    {
        var unit = other.gameObject.GetComponent<Unit>();
        if(unit != null && unit.IsActive)
        {
            unit.transform.parent = transform;
            var randomCirclePoint = Random.insideUnitCircle * 0.75f;
            var insideUnitCircle = new Vector3(transform.position.x + randomCirclePoint.x, transform.position.y, transform.position.z + randomCirclePoint.y);
            unit.gameObject.transform.position = insideUnitCircle;
            // unit.gameObject.transform.DOMove(insideUnitCircle, 2f);
            unit.TryToSave(ToolType.Boat);
        }
    }
}
