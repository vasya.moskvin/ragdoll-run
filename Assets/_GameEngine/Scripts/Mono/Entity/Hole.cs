using System;
using UnityEngine;

public class Hole : MonoBehaviour
{
    [SerializeField]
    private GameObject _aim;

    [SerializeField]
    private GameObject _hole;

    [SerializeField]
    private ParticleSystem _dust;

    private LevelModel _levelModel;

    public void SetLevelModel(LevelModel levelModel)
    {
        _levelModel = levelModel;
    }

    public void Success()
    {
        _aim.SetActive(false);
        _hole.SetActive(true);
    }

    public void StartDust()
    {
        if (_levelModel != null &&
            _levelModel.CurrentHoleModel != null &&
            _levelModel.CurrentHoleModel.GetProgress() > 0.05f)
        {
            _dust.Play();
        }
        else
        {
            Invoke(nameof(StartDust), 0.1f);
        }
    }

    public void StopDust()
    {
        CancelInvoke(nameof(StartDust));
        _dust.Stop();
    }

    private void Start()
    {
        _aim.SetActive(false);
        _hole.SetActive(false);
        _dust.Stop();
    }
}