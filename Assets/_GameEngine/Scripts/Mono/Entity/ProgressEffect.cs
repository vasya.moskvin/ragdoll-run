using UnityEngine;

public class ProgressEffect : MonoBehaviour
{
    [SerializeField]
    private Transform _target;

    [SerializeField, Range(0, 1)]
    private float _startingPercent;

    [SerializeField]
    private float _maxScale;

    [SerializeField]
    private AnimationCurve _curve;

    public void SetProgress(float progress)
    {
        if (progress < _startingPercent)
        {
            _target.localScale = Vector3.zero;
        }
        else
        {
            progress = Mathf.Clamp01((progress - _startingPercent) / (1 - _startingPercent));
            var scale = _curve.Evaluate(progress) * _maxScale;
            _target.localScale = new Vector3(scale, scale, scale);
        }
    }
}
