using DG.Tweening;
using UnityEngine;

public class Shakeable : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _rigidbody;

    private bool _isDelay;

    public void AddForce(float force)
    {
        if (force < Random.Range(0, 0.5f) || _isDelay)
        {
            return;
        }

        _isDelay = true;
        transform.DOShakePosition(1f, 0.3f, 2).OnComplete(
            () =>
            {
                _rigidbody.AddForce(new Vector3(0, Random.Range(0, 0.3f), 0), ForceMode.Impulse);
                _isDelay = false;
            });
    }
}