using System.Collections;
using System.Linq;
using GameEngine.Menu.Common;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField]
    private GameSettings _gameSettings;

    [SerializeField]
    private Menu _menu;

    [SerializeField]
    private LoadingScenes _loadingScenes;

    private static Game instance;

    public LevelSettings GetCurrentLevel()
    {
        if (!string.IsNullOrEmpty(AccountData.GetAccountModel().NextIdLevel))
        {
            var currentLevel = _gameSettings.Levels.FirstOrDefault(level => level.Id.Equals(AccountData.GetAccountModel().NextIdLevel));
            if (currentLevel != null)
            {
                return currentLevel;
            }
        }

        return _gameSettings.Levels[AccountData.GetAccountModel().NextLevelIndex % _gameSettings.Levels.Count];
    }

    private void Start()
    {
        instance = this;
        // _loadingScenes.TryIndexLevel(AccountData.GetAccountModel().NextLevelIndex);
        // InitMainMenu();
        AnalyticsHelper.Initialize();
        StartLevel();
    }

    public void StartLevel()
    {
        TimeScaleManager.SetBase();
        var level = GetCurrentLevel();
        var levelModel = new LevelModel(AccountData.GetAccountModel().NextLevelNumber, level);
        levelModel.MaxNoise = 3f;
        levelModel.MaxForce = 3f;
        _loadingScenes.TryLoadLevel(level.SceneName);

        StartCoroutine(InitLevelAsync(level, levelModel));
    }

    public void CancelLevel()
    {
        Clear();
        InitMainMenu();
    }

    private void InitMainMenu()
    {
        _menu.Show(ScreenType.Main, true, new ScreenArguments { Game = this });
        TimeScaleManager.SetBase();
    }

    private IEnumerator InitLevelAsync(LevelSettings level, LevelModel levelModel)
    {
        Clear();
        yield return null;
        InitLevel(level, levelModel);
        while (!LevelHelper.GetLevel().IsInitialized)
        {
            yield return null;
        }

        var tutorialModel = new TutorialModel();
        LevelHelper.GetLevel().LevelEnded += OnLevelEnded;
        LevelHelper.GetLevel().SetModel(levelModel);
        LevelHelper.GetLevel().SetTutorialModel(tutorialModel);
        LevelHelper.GetLevel().SetMainCamera(Camera.main);
        LevelHelper.GetLevel().Init();
        AnalyticsHelper.SendStartLevelEvent(levelModel.LevelNumber);

        _menu.Show(
            ScreenType.Game,
            true,
            new ScreenArguments
            {
                Game = this,
                Level = levelModel,
                Camera = Camera.main,
                TutorialModel = tutorialModel
            });
    }

    private void OnLevelEnded(bool success, LevelModel levelModel)
    {
        LevelHelper.GetLevel().LevelEnded -= OnLevelEnded;
        if (success)
        {
            AnalyticsHelper.SendWinLevelEvent(levelModel.LevelNumber);
            OnLevelCompleted(levelModel);
        }
        else
        {
            AnalyticsHelper.SendFailLevelEvent(levelModel.LevelNumber);
            LevelFailed();
        }
    }

    private void LevelFailed()
    {
        //
        _menu.Show(ScreenType.Lose, true, new ScreenArguments { Game = this });
    }

    private void Clear()
    {
        //todo
    }

    private void InitLevel(LevelSettings level, LevelModel model)
    {
        //todo
    }

    public void OnLevelCompleted(LevelModel levelModel)
    {
        var rewardCoins = CalculateRewardCoins(levelModel);
        AccountData.GetAccountModel().AddCoins(rewardCoins);
        AccountData.GetAccountModel().FinishLevel();
        AccountData.GetAccountModel().NextIdLevel = CalculateNextLevelId(levelModel);
        AccountData.Save();
        TimeScaleManager.SetBase();
        StartCoroutine(ShowWinScreenDelay(rewardCoins));
    }

    public IEnumerator ShowWinScreenDelay(int rewardCoins)
    {
        yield return null;
        _menu.Show(ScreenType.Win, true, new ScreenArguments { Game = this, RewardCoins = rewardCoins });
    }

    private string CalculateNextLevelId(LevelModel levelModel)
    {
        if (levelModel.LevelNumber >= _gameSettings.Levels.Count)
        {
            var lastId = levelModel.LevelSettings.Id;
            var availableLevels = _gameSettings.Levels.Where(level => !level.Id.Equals(lastId)).Select(l => l.Id).ToList();
            var randomIndex = Random.Range(0, availableLevels.Count);
            return availableLevels[randomIndex];
        }
        else
        {
            return _gameSettings.Levels[AccountData.GetAccountModel().NextLevelIndex % _gameSettings.Levels.Count].Id;
        }
    }

    private int CalculateRewardCoins(LevelModel levelModel)
    {
        return levelModel.LevelNumber * 10;
    }
}