using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.UI;

public class Unit : MonoBehaviour
{
    [SerializeField]
    private UnitRenderer _unitRenderer;

    [SerializeField]
    private Mover _mover;

    [SerializeField]
    private Transform _headTransform;

    [SerializeField]
    private Image _progress;

    [SerializeField]
    private Rigidbody _rigidbody;

    [SerializeField]
    private Animator _animator;

    [SerializeField]
    private List<ColorArrow> _colorMap;

    public event Action Died;
    public event Action Rescued;

    private bool _isActive = true;
    private float _lifetime;
    private float _currentTime;
    private TweenerCore<float, float, FloatOptions> _timer;
    private static readonly int IsClimbing = Animator.StringToHash("IsClimbing");
    private static readonly int StopClimb = Animator.StringToHash("StopClimb");

    public bool IsActive => _isActive;

    public void Init(float lifetime, float currentTime)
    {
        _lifetime = lifetime;
        _currentTime = currentTime;
        _timer = DOTween.To(GetTime, SetTime, _lifetime, _lifetime - _currentTime)
                        .SetEase(Ease.Linear)
                        .OnUpdate(UpdateProgress)
                        .OnComplete(() => { TryDie(GlobalSettings.DieVibration); });
    }

    private float GetTime()
    {
        return _currentTime;
    }

    private void SetTime(float time)
    {
        _currentTime = time;
    }

    public void PauseTimer()
    {
        _timer.Pause();
    }

    public void SharkBite()
    {
        _timer.Kill();
        VibrationMediator.Vibrate(GlobalSettings.ChangeVibration);
        _currentTime = Mathf.Max(_lifetime - 15f, _currentTime);
        _timer = DOTween.To(GetTime, SetTime, _lifetime, _lifetime - _currentTime)
                        .SetEase(Ease.Linear)
                        .OnUpdate(UpdateProgress)
                        .OnComplete(() => { TryDie(GlobalSettings.DieVibration); });
    }

    public void PlayTimer()
    {
        _timer.Play();
    }

    public float GetProgress()
    {
        return _currentTime / _lifetime;
    }

    private void UpdateProgress()
    {
        _progress.fillAmount = 1 - GetProgress();
        _progress.color = GetColorProgress();
    }

    public Color GetColorProgress()
    {
        return GetColor(GetTimerType());
    }

    private TimerType GetTimerType()
    {
        var progress = GetProgress();
        if (progress < 0.5f)
        {
            return TimerType.Long;
        }

        if (progress < 0.75f)
        {
            return TimerType.Medium;
        }

        return TimerType.Fast;
    }

    public void Move(List<Vector3> points)
    {
        //todo add move to boat
    }

    private void OnTriggerEnter(Collider other)
    {
        //todo implement collide boat/lifebuoy/rope
    }

    public void TryDie(VibrationSettings vibrationSettings)
    {
        if (_isActive)
        {
            _isActive = false;
            gameObject.SetActive(false);
            Died?.Invoke();
            VibrationMediator.Vibrate(vibrationSettings);
        }
    }

    public void TryToSave(ToolType tool)
    {
        if (_isActive)
        {
            _isActive = false;
            _timer.Kill();
            _progress.gameObject.SetActive(false);
            _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            VibrationMediator.Vibrate(GlobalSettings.SaveVibration);
            Rescued?.Invoke();
            PlaySaveAnimation(tool);
        }
    }

    private void PlaySaveAnimation(ToolType tool)
    {
        switch (tool)
        {
            case ToolType.Lifebuoy:
                break;
            case ToolType.Boat:
                break;
            case ToolType.Rope:
                // _animator.SetTrigger(IsClimbing);
                // _animator.applyRootMotion = true;
                DOTween.To(GetTakingLayerAnimator, SetTakingLayerAnimator, 1, 0.5f);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(tool), tool, null);
        }
    }

    private float GetTakingLayerAnimator()
    {
        return _animator.GetLayerWeight(1);
    }

    private void SetTakingLayerAnimator(float value)
    {
        _animator.SetLayerWeight(1, value);
    }


    private Color GetColor(TimerType type)
    {
        foreach (var arrow in _colorMap.Where(arrow => arrow.Type == type))
        {
            return arrow.Color;
        }

        return Color.black;
    }
}

public enum TimerType
{
    Fast,
    Medium,
    Long
}

[Serializable]
public class ColorArrow
{
    [SerializeField]
    private TimerType _type;

    [SerializeField]
    private Color _color;

    public TimerType Type => _type;

    public Color Color => _color;
}