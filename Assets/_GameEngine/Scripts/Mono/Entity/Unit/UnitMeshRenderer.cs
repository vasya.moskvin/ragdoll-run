using UnityEngine;

public class UnitMeshRenderer : UnitRenderer
{
    [SerializeField]
    private MeshRenderer _body;

    public override void SetMaterial(Material material)
    {
        _body.material = material;
    }
}
