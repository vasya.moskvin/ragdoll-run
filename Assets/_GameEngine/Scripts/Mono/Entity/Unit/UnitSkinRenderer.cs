using UnityEngine;

public class UnitSkinRenderer : UnitRenderer
{
    [SerializeField]
    private SkinnedMeshRenderer _body;

    public override void SetMaterial(Material material)
    {
        _body.material = material;
    }
}
