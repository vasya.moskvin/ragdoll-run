using System.Collections;
using DG.Tweening;
using UnityEngine;

public class UnitEffect : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup _canvasGroup;

    public IEnumerator Start()
    {
        _canvasGroup.alpha = 1;
        yield return null;
        _canvasGroup.DOFade(0, 2f)
                    .SetEase(Ease.Linear)
                    .OnComplete(() => Destroy(gameObject));
    }
}
