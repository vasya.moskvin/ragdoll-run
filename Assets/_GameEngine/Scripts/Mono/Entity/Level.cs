using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Level : MonoBehaviour, ILevel
{
    public event Action<bool, LevelModel> LevelEnded;

    public bool IsInitialized { get; private set; }

    [SerializeField]
    private Drill _drill = default;

    [SerializeField]
    private Transform _drillCameraPoint = default;

    [SerializeField]
    private GameObject _winEffect = default;

    [SerializeField]
    private GameObject _loseEffect = default;

    [SerializeField]
    private EffectContainer _progressEffect = default;

    [SerializeField]
    private List<float> _boundForProgressEffect = default;

    [SerializeField]
    private List<PropsContainer> _props;

    private LevelModel _levelModel;
    private DrillZone _drillZone = default;
    private Vector3 _startCameraPosition;
    private Quaternion _startCameraRotation;
    private Camera _mainCamera;
    private readonly Vector3 _offset = - Vector3.up * 0.35f;
    private Hole _currentHole;
    private int _indexProgressEffect;

    public void SetModel(LevelModel levelModel)
    {
        _levelModel = levelModel;
        _levelModel.NoiseEmpty += OnNoiseEmpty;
        _levelModel.NoisePeaked += OnNoisePeaked;
        _levelModel.ForcePeaked += OnForcePeaked;
        _levelModel.GoalFinished += OnGoalFinish;
        _levelModel.NoiseChanged += UpdateProgress;
        _drillZone = Instantiate(_levelModel.LevelSettings.Wall).DrillZone;
        _drill.Init(_levelModel);
        _currentHole = _drillZone.GetRandomHole();
        _currentHole.SetLevelModel(_levelModel);
        _drill.MoveTo(_currentHole.transform.position, _offset);
        _levelModel.DrillStarted += OnDrillStarted;
        _levelModel.DrillButtonUp += OnDrillButtonUp;
        _levelModel.DrillButtonDown += OnDrillButtonDown;
        foreach (var container in _props)
        {
            container.SetModel(_levelModel);
        }
    }

    public void SetTutorialModel(TutorialModel tutorialModel) { }

    public void SetMainCamera(Camera mainCamera)
    {
        _mainCamera = mainCamera;
        _startCameraPosition = mainCamera.transform.position;
        _startCameraRotation = mainCamera.transform.rotation;
    }

    public void Init()
    {
    }

    private void Awake()
    {
        IsInitialized = true;
        LevelHelper.SetLevel(this);
        _winEffect.SetActive(false);
        _loseEffect.SetActive(false);
    }

    private void OnForcePeaked()
    {
        LevelEnd(false);
    }

    private void UpdateProgress()
    {
        if (_indexProgressEffect < _boundForProgressEffect.Count)
        {
            var noiseProgress = _levelModel.GetNoiseProgress();
            if (noiseProgress > _boundForProgressEffect[_indexProgressEffect])
            {
                _progressEffect.gameObject.SetActive(true);
                _progressEffect.Play();
                _indexProgressEffect++;
            }
        }
    }

    private void OnDrillButtonDown()
    {
        if (_currentHole != null)
        {
            _currentHole.StartDust();
        }
    }

    private void OnDrillButtonUp()
    {
        if (_currentHole != null)
        {
            _currentHole.StopDust();
        }
    }

    private void OnNoisePeaked()
    {
        LevelEnd(true);
    }

    private void OnDrillStarted(HoleModel holeModel)
    {
        _mainCamera.transform.DOMove(_drillCameraPoint.position, 1f);
        _mainCamera.transform.DORotateQuaternion(_drillCameraPoint.rotation, 1f);
    }

    private void OnGoalFinish()
    {
        _currentHole.Success();
        _currentHole.StopDust();

        _currentHole = _drillZone.GetRandomHole();
        _currentHole.SetLevelModel(_levelModel);

        _drill.ResetPosition();
        _drill.MoveTo(_currentHole.transform.position, _offset);
    }

    private void OnNoiseEmpty()
    {
        LevelEnd(false);
    }

    private void LevelEnd(bool isSuccess)
    {
        Unsubscribe();
        LevelEnded?.Invoke(isSuccess, _levelModel);
        _winEffect.SetActive(isSuccess);
        _loseEffect.SetActive(!isSuccess);
    }

    private void Unsubscribe()
    {
        if (_levelModel != null)
        {
            _levelModel.NoisePeaked -= OnNoiseEmpty;
        }
    }
}

public interface ILevel
{
    bool IsInitialized { get; }
    void SetModel(LevelModel levelModel);
    void SetMainCamera(Camera mainCamera);
    void Init();
    event Action<bool, LevelModel> LevelEnded;
    void SetTutorialModel(TutorialModel tutorialModel);
}

public class EmptyLevel : ILevel
{
    public bool IsInitialized => false;
    public void Init()
    {
        if (IsInitialized)
        {
            LevelEnded?.Invoke(true, null);
        }
    }
    public event Action<bool, LevelModel> LevelEnded;
    public void SetTutorialModel(TutorialModel tutorialModel)
    {
    }

    public void SetModel(LevelModel levelModel)
    {
    }

    public void SetMainCamera(Camera mainCamera)
    {
    }
}

public static class LevelHelper
{
    private static readonly ILevel Empty = new EmptyLevel();

    private static ILevel Level;

    public static void SetLevel(ILevel level)
    {
        Level = level;
    }

    public static ILevel GetLevel()
    {
        return Level ?? Empty;
    }
}