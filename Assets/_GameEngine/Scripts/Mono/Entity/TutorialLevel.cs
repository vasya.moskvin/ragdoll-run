using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TutorialLevel : MonoBehaviour, ILevel
{
    public event Action<bool, LevelModel> LevelEnded;

    public bool IsInitialized { get; private set; }

    [SerializeField]
    private Drill _drill = default;

    [SerializeField]
    private Transform _drillCameraPoint = default;

    [SerializeField]
    private GameObject _winEffect = default;

    [SerializeField]
    private GameObject _loseEffect = default;

    [SerializeField]
    private EffectContainer _progressEffect = default;

    [SerializeField]
    private List<float> _boundForProgressEffect = default;

    [SerializeField]
    private List<PropsContainer> _props;

    private LevelModel _levelModel;

    private DrillZone _drillZone = default;

    private Vector3 _startCameraPosition;

    private Quaternion _startCameraRotation;

    private Camera _mainCamera;

    private readonly Vector3 _offset = - Vector3.up * 0.35f;

    private Hole _currentHole;

    private int _indexProgressEffect;

    private TutorialState _tutorialState = TutorialState.DrillOne;
    private TutorialModel _tutorialModel;
    private bool _isNeedCheckBreaking = true;

    public void SetModel(LevelModel levelModel)
    {
        _levelModel = levelModel;
        _levelModel.NoiseEmpty += OnNoiseEmpty;
        _levelModel.NoisePeaked += OnNoisePeaked;
        _levelModel.ForcePeaked += OnForcePeaked;
        _levelModel.GoalFinished += OnGoalFinish;
        _levelModel.NoiseChanged += UpdateProgress;
        _drillZone = Instantiate(_levelModel.LevelSettings.Wall).DrillZone;
        _drill.Init(_levelModel);
        _currentHole = _drillZone.GetRandomHole();
        _currentHole.SetLevelModel(_levelModel);
        _drill.MoveTo(_currentHole.transform.position, _offset);
        _levelModel.DrillStarted += OnDrillStarted;
        _levelModel.DrillStarted += OnTutorialStarted;
        _levelModel.DrillButtonUp += OnDrillButtonUp;
        _levelModel.DrillButtonDown += OnDrillButtonDown;
        _levelModel.MaxForce = 5;
        foreach (var container in _props)
        {
            container.SetModel(_levelModel);
        }
    }

    public void SetTutorialModel(TutorialModel tutorialModel)
    {
        _tutorialModel = tutorialModel;
    }

    public void SetMainCamera(Camera mainCamera)
    {
        _mainCamera = mainCamera;
        _startCameraPosition = mainCamera.transform.position;
        _startCameraRotation = mainCamera.transform.rotation;
    }

    public void Init()
    {
    }

    private void OnTutorialStarted(HoleModel obj)
    {
        _levelModel.DrillStarted -= OnTutorialStarted;
        SetTutorialState(TutorialState.DrillOne);
    }

    private void SetTutorialState(TutorialState state)
    {
        _tutorialState = state;
        switch (state)
        {
            case TutorialState.DrillOne:
                _tutorialModel.SetText("Hold to drill");
                _tutorialModel.SetNeedPressUp(false);
                _tutorialModel.SetNeedPressDown(true);
                break;
            case TutorialState.BreakingDrill:
                _tutorialModel.SetText("Let the drill rests");
                _tutorialModel.SetNeedPressUp(true);
                _tutorialModel.SetNeedPressDown(false);
                Time.timeScale = 0;
                break;
            case TutorialState.DrillTwo:
                _tutorialModel.SetText("Hold to drill");
                _tutorialModel.SetNeedPressUp(false);
                _tutorialModel.SetNeedPressDown(true);
                Time.timeScale = 1;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }

    private void ChangeHandState(bool isActive)
    {
        switch (_tutorialState)
        {
            case TutorialState.DrillOne:
                _tutorialModel.SetNeedPressUp(false);
                _tutorialModel.SetNeedPressDown(isActive);
                break;
            case TutorialState.DrillTwo:
                _tutorialModel.SetNeedPressUp(false);
                _tutorialModel.SetNeedPressDown(isActive);
                break;
        }
    }

    private void Awake()
    {
        IsInitialized = true;
        LevelHelper.SetLevel(this);
        _winEffect.SetActive(false);
        _loseEffect.SetActive(false);
    }

    private void Update()
    {
        if (_tutorialState == TutorialState.BreakingDrill)
        {
            if (Input.GetMouseButtonUp(0))
            {
                StartCoroutine(SetDrillTwoState());
            }
        }
    }

    private void OnForcePeaked()
    {
        // LevelEnd(false);
    }

    private void UpdateProgress()
    {
        if (_indexProgressEffect < _boundForProgressEffect.Count)
        {
            var noiseProgress = _levelModel.GetNoiseProgress();
            if (noiseProgress > _boundForProgressEffect[_indexProgressEffect])
            {
                _progressEffect.gameObject.SetActive(true);
                _progressEffect.Play();
                _indexProgressEffect++;
            }
        }

        if (_isNeedCheckBreaking)
        {
            var noiseProgress = _levelModel.GetNoiseProgress();
            if (noiseProgress >= 0.85)
            {
                _isNeedCheckBreaking = false;
                _drill.IsBlocking = true;
                _levelModel.MaxForce = _levelModel.Force - 0.1f;
                SetTutorialState(TutorialState.BreakingDrill);
            }
        }
    }

    private void OnDrillButtonDown()
    {
        if (_currentHole != null)
        {
            _currentHole.StartDust();
        }

        ChangeHandState(false);
    }

    private void OnDrillButtonUp()
    {
        if (_currentHole != null)
        {
            _currentHole.StopDust();
        }

        ChangeHandState(true);
    }

    private IEnumerator SetDrillTwoState()
    {
        yield return new WaitForSecondsRealtime(0.5f);

        _levelModel.Force = 0;
        _levelModel.MaxForce = 3;
        _levelModel.ReloadDrill();
        _drill.IsBlocking = false;
        SetTutorialState(TutorialState.DrillTwo);
    }

    private void OnNoisePeaked()
    {
        LevelEnd(true);
    }

    private void OnDrillStarted(HoleModel holeModel)
    {
        _mainCamera.transform.DOMove(_drillCameraPoint.position, 1f);
        _mainCamera.transform.DORotateQuaternion(_drillCameraPoint.rotation, 1f);
    }

    private void OnGoalFinish()
    {
        _currentHole.Success();
        _currentHole.StopDust();

        _currentHole = _drillZone.GetRandomHole();
        _currentHole.SetLevelModel(_levelModel);

        _drill.ResetPosition();
        _drill.MoveTo(_currentHole.transform.position, _offset);
    }

    private void OnNoiseEmpty()
    {
        LevelEnd(false);
    }

    private void LevelEnd(bool isSuccess)
    {
        Unsubscribe();
        LevelEnded?.Invoke(isSuccess, _levelModel);
        _winEffect.SetActive(isSuccess);
        _loseEffect.SetActive(!isSuccess);
    }

    private void Unsubscribe()
    {
        if (_levelModel != null)
        {
            _levelModel.NoisePeaked -= OnNoiseEmpty;
        }
    }
}
