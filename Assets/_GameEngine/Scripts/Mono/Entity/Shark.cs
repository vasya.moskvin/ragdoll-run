using System;
using System.Collections;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

public class Shark : MonoBehaviour
{
    private float _radius = 2.5f;
    private float _angle;
    private TweenerCore<float, float, FloatOptions> _circleMoving;
    private Sequence _sequence;

    public void Select(Unit unit)
    {
        transform.LookAt(unit.transform.position);
        _sequence = DOTween.Sequence();
        // var moveTo = transform.DOMove(transform.position + 0.75f * (unit.position - transform.position), 1f)
        //                       .SetEase(Ease.Linear);
        // var downMove = transform.DOMove(unit.position + Vector3.down, 0.5f)
        //                         .SetEase(Ease.Linear);

        // var circleMoving = 
        var newPosition = GetFirstAngle(unit.transform);
        var moveTo = transform.DOMove(newPosition, 3f)
                              .SetEase(Ease.Linear);
        _sequence.Append(moveTo);
        // sequence.Append(downMove);
        // sequence.Append(circleMoving);
        _sequence.OnUpdate(
                    () =>
                    {
                        if (!unit.IsActive)
                        {
                            Destroy(gameObject);
                        }
                    })
                .OnComplete(
                    () =>
                    {
                        unit.SharkBite();
                        _circleMoving = DOTween.To(() => _angle, value => _angle = value, _angle + 2 * Mathf.PI, 4f)
                                               .OnUpdate(
                                                   () =>
                                                   {
                                                       if (!unit.IsActive)
                                                       {
                                                           Destroy(gameObject);
                                                           return;
                                                       }
                                                       var newPosition = new Vector3(
                                                           unit.transform.position.x + Mathf.Cos(_angle) * _radius,
                                                           transform.position.y,
                                                           unit.transform.position.z + Mathf.Sin(_angle) * _radius);

                                                       transform.LookAt(newPosition);
                                                       transform.position = newPosition;
                                                   })
                                               .SetEase(Ease.Linear)
                                               .SetLoops(-1, LoopType.Restart);
                        // StartCoroutine(CircleMoving(unit));
                    });
    }

    private Vector3 GetFirstAngle(Transform unit)
    {
        var minDistance = float.MaxValue;
        for (var angle = 0f; angle < 2 * Mathf.PI; angle += Mathf.PI / 6f)
        {
            var position = new Vector3(
                unit.position.x + Mathf.Cos(angle) * _radius,
                transform.position.y,
                unit.position.z + Mathf.Sin(angle) * _radius);

            var magnitude = (transform.position - position).magnitude;
            if (magnitude < minDistance)
            {
                minDistance = magnitude;
                _angle = angle;
            }

        }

        return new Vector3(
            unit.position.x + Mathf.Cos(_angle) * _radius,
            transform.position.y,
            unit.position.z + Mathf.Sin(_angle) * _radius);
    }

    private void OnDestroy()
    {
        if (_sequence != null)
        {
            _sequence.Kill();
        }

        if (_circleMoving != null)
        {
            _circleMoving.Kill();
        }
    }
}