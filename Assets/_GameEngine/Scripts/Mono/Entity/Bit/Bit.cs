using UnityEngine;

public class Bit : MonoBehaviour
{
    [SerializeField]
    private Renderer _renderer;

    [SerializeField]
    private Gradient _gradient;

    public void ChangeColor(float progress)
    {
        progress = Mathf.Clamp01(progress);
        _renderer.materials[0].SetColor("_Color", _gradient.Evaluate(progress));
    }
}
