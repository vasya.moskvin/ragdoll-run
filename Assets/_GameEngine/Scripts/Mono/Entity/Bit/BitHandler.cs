using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BitHandler : MonoBehaviour
{
    [SerializeField]
    private Transform _rotateBit;

    [SerializeField]
    private Transform _bitPlace;

    [SerializeField]
    private Image _indicator;

    [SerializeField]
    private Gradient _indicatorGradient;

    [SerializeField]
    private List<ProgressEffect> _progressEffects;

    private float _speed;
    private Bit _bit;
    private const float MultiplierSpeed = 400;
    private LevelModel _levelModel;

    public void SetModel(LevelModel model)
    {
        _levelModel = model;
        _levelModel.ForceChanged += OnForceChanged;
        _levelModel.BitChanged += OnBitChanged;
        SetBitSettings(_levelModel.BitSettings);
        _indicator.color = _indicatorGradient.Evaluate(0);
        foreach (var effect in _progressEffects)
        {
            effect.SetProgress(0);
        }
    }

    private void SetBitSettings(BitSettings bitSettings)
    {
        if (_bit != null)
        {
            Destroy(_bit);
            _bit = null;
        }

        _bit = Instantiate(bitSettings.Body, _bitPlace.position, Quaternion.identity, _rotateBit);
        _bit.transform.localEulerAngles = Vector3.zero;
    }

    private void OnBitChanged()
    {
        SetBitSettings(_levelModel.BitSettings);
    }

    private void OnForceChanged()
    {
        _speed = _levelModel.Force * MultiplierSpeed;
        var progress = _levelModel.Force / _levelModel.MaxForce;

        if (_bit != null)
        {
            _bit.ChangeColor(progress);
        }

        _indicator.color = _indicatorGradient.Evaluate(progress);
        foreach (var effect in _progressEffects)
        {
            effect.SetProgress(progress);
        }
    }

    private void Update()
    {
        _rotateBit.transform.Rotate(0, 0, -_speed * Time.deltaTime);
    }

    private void OnDestroy()
    {
        _levelModel.ForceChanged -= OnForceChanged;
        _levelModel.BitChanged -= OnBitChanged;
    }
}