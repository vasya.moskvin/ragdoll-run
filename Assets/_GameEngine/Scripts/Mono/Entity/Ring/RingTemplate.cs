using System.Collections.Generic;
using UnityEngine;

public class RingTemplate : MonoBehaviour
{
    [SerializeField]
    private List<RingSector> _sectors;

    public void Init(List<MultiplySkin> skins, List<int> multiplies)
    {
        for (var index = 0; index < _sectors.Count; index++)
        {
            var multiplySkin = skins[index % skins.Count];
            var ringViewType = multiplySkin.ColorType == ColorType.Red
                                   ? RingViewType.Image
                                   : RingViewType.Text;

            _sectors[index].Init(multiplySkin, multiplies[index % multiplies.Count], ringViewType);
        }
    }
}