using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RingSector : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer _meshRenderer;

    [SerializeField]
    private TMP_Text _text;

    [SerializeField]
    private Image _statusImage;

    private MultiplySkin _multiplySkin;
    private int _multiply;

    public MultiplySkin MultiplySkin => _multiplySkin;

    public int Multiply => _multiply;

    public void Init(MultiplySkin skin, int multiply, RingViewType ringViewType)
    {
        _multiplySkin = skin;
        _multiply = multiply;
        _statusImage.gameObject.SetActive(ringViewType == RingViewType.Image);
        _text.gameObject.SetActive(ringViewType == RingViewType.Text);
        switch (ringViewType)
        {
            case RingViewType.Text:
                _text.text = $"x{multiply}";
                break;
            case RingViewType.Image:
                _statusImage.sprite = skin.Sprite;
                _statusImage.color = Color.white;
                break;
        }

        if (_multiplySkin != null)
        {
            _meshRenderer.material = _multiplySkin.MaterialRing;
        }
    }
}

public enum RingViewType
{
    Text = 0,
    Image = 1
}