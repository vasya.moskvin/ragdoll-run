using UnityEngine;

public class RingRotator : MonoBehaviour
{
#if UNITY_EDITOR
    private int _rotationSpeed = 120;
#endif

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, 0 ,_rotationSpeed * Time.deltaTime);
            return;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, 0, -_rotationSpeed * Time.deltaTime);
            return;
        }
#endif
        if (Input.touchCount > 0)
        {
            var firstFinger = Input.GetTouch(0);

            if (firstFinger.phase == TouchPhase.Moved)
            {
                var speed = firstFinger.deltaPosition.x / Screen.width;
                transform.Rotate(0, 0, -360 * speed);
            }
        }
    }
}