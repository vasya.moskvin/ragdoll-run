using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PropsContainer : MonoBehaviour
{
    [SerializeField]
    private List<Shakeable> _props = new List<Shakeable>();

    private LevelModel _levelModel;

    public void SetModel(LevelModel levelModel)
    {
        _levelModel = levelModel;
        _levelModel.ForceChanged += OnForceChanged;
    }

    [ContextMenu("Search Props")]
    private void SearchProps()
    {
#if UNITY_EDITOR
        _props.Clear();
        _props = GetComponentsInChildren<Shakeable>().ToList();
        EditorUtility.SetDirty(gameObject);
#endif
    }

    private void OnForceChanged()
    {
        foreach (var shakeable in _props)
        {
            shakeable.AddForce(_levelModel.Force);
        }
    }
}
