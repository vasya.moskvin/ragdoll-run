using UnityEngine;

public class TimeScaleManager
{
    private const float BASE_TIME_SCALE = 1f;

    public static void SetTimeScale(float timeScale)
    {
        Time.timeScale = timeScale;
    }

    public static float GetTimeScale()
    {
        return Time.timeScale;
    }

    public static void SetBase()
    {
        SetTimeScale(BASE_TIME_SCALE);
    }
}
