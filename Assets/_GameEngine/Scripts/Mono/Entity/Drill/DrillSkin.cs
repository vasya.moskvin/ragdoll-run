using UnityEngine;

public class DrillSkin : MonoBehaviour
{
    [SerializeField]
    private Renderer _body;

    public void Init(Material main, Material second)
    {
        _body.materials = new[] { main, second };
    }
}
