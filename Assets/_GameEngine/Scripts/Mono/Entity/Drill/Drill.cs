using System;
using System.Runtime.InteropServices.WindowsRuntime;
using DG.Tweening;
using UnityEngine;

[DisallowMultipleComponent]
public class Drill : MonoBehaviour
{
    [SerializeField]
    private BitHandler _bitHandler;

    [SerializeField]
    private DrillSkin _drillSkin;

    [SerializeField]
    private AudioSource _audioSource;

    public bool IsBlocking { get; set; }

    private LevelModel _level;
    private Vector3 _startPosition;
    private Vector3 _endPosition;
    private Vector3 _startRotation;
    private bool _isProcessing;
    private bool _isMoving;
    private HoleModel _holeModel;
    private Sequence _animationSequence;
    private Sequence _idleAnimation;
    private Sequence _prepareIdle;
    private bool _isNeedStartDrill;

    public void Init(LevelModel level)
    {
        _level = level;
        _level.ForceChanged += OnNoiseChanged;
        _level.DrillStarted += OnDrillStarted;
        _level.DrillButtonDown += OnDrillButtonDown;
        _level.DrillButtonUp += OnDrillButtonUp;
        _bitHandler.SetModel(level);
        _audioSource.clip = _level.LevelSettings.DrillAudioClip;
        _drillSkin.Init(_level.LevelSettings.DrillMaterialFirst, _level.LevelSettings.DrillMaterialSecond);
    }

    public void ResetPosition()
    {
        MoveTo(_startPosition, Vector3.zero, 0.5f);
    }

    public void MoveTo(Vector3 target,
                       Vector3 offset,
                       float duration = 2f)
    {
        _isMoving = true;
        var moveTo = new Vector3(target.x, target.y, _startPosition.z) + offset;
        var move = transform.DOMove(moveTo, duration);
        if (_animationSequence != null)
        {
            _animationSequence.Append(move);
        }
        else
        {
            CancelIdleAnimation();
            _animationSequence = DOTween.Sequence();
            _animationSequence.Append(move);
            _animationSequence.OnComplete(
                () =>
                {
                    _startPosition = transform.position;
                    _animationSequence = null;
                    _isMoving = false;
                    if (_isNeedStartDrill)
                    {
                        _isNeedStartDrill = false;
                        StartDrill();
                    }
                    else
                    {
                        StartIdleAnimation();
                    }
                });
        }
    }

    public void StartDrilling()
    {
        _startRotation = transform.localEulerAngles;
        transform.DORotate(Vector3.zero, 1f);
    }

    private void StartIdleAnimation()
    {
        if (_idleAnimation != null)
        {
            _idleAnimation.Kill();
        }

        if (_prepareIdle != null)
        {
            _prepareIdle.Kill();
        }

        var up = transform.position + Vector3.up * 0.05f;
        var down = transform.position + Vector3.down * 0.05f;

        _prepareIdle = DOTween.Sequence();
        _prepareIdle.Append(transform.DOMove(up, 0.5f));

        _idleAnimation = DOTween.Sequence();
        _idleAnimation.Append(transform.DOMove(down, 1f));
        _idleAnimation.Append(transform.DOMove(up, 1f));
        _idleAnimation.SetLoops(-1, LoopType.Yoyo);
        _idleAnimation.SetDelay(0.5f);
    }

    private void CancelIdleAnimation()
    {
        if (_idleAnimation != null)
        {
            _idleAnimation.Kill();
            _idleAnimation = null;
            transform.DOMove(_startPosition, 0.1f);
        }

        if (_prepareIdle != null)
        {
            _prepareIdle.Kill();
            _prepareIdle = null;
        }
    }

    private void OnDrillButtonUp()
    {
        _audioSource.Stop();
        TryStartIdleAnimation();
    }

    private void TryStartIdleAnimation()
    {
        if (_holeModel != null && _holeModel.GetProgress() < 0.1f && !_isMoving)
        {
            StartIdleAnimation();
        }
    }

    private void OnDrillButtonDown()
    {
        _audioSource.Play();
        CancelIdleAnimation();
    }

    private void Start()
    {
        _startPosition = transform.position;
    }

    private void OnDrillStarted(HoleModel holeModel)
    {
        _holeModel = holeModel;
        if (_animationSequence != null)
        {
            _isNeedStartDrill = true;
        }
        else
        {
            StartDrill();
        }
    }

    private void StartDrill()
    {
        _startPosition = transform.position;
        _endPosition = _startPosition + Vector3.forward * 0.4f;
        StartDrilling();
        StartIdleAnimation();
        _isProcessing = true;
    }

    private void OnNoiseChanged()
    {
        var amplitude = (int) Mathf.Clamp(_level.Force * 40, 0, 255);
        VibrationMediator.Vibrate(new VibrationSettings { Duration = 80, Amplitude = amplitude });
    }

    private void Update()
    {
        if (IsBlocking)
        {
            return;
        }

        if (_isProcessing)
        {
            _audioSource.volume = Mathf.Clamp01(0.5f + _level.Force / _level.MaxForce);
            if (!_isMoving)
            {
                _endPosition = _startPosition + Vector3.forward * 0.4f;
                var progress = _holeModel.GetProgress();
                transform.position = Vector3.Lerp(_startPosition, _endPosition, progress);

                if (Input.GetMouseButton(0))
                {
                    _level.Drill(Time.deltaTime);
                }
                else
                {
                    _level.NotDrill(Time.deltaTime);
                }
            }
            else
            {
                _level.NotDrill(Time.deltaTime);
            }
        }
    }
}