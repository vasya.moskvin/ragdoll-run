using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class DrillZone : MonoBehaviour
{
    [SerializeField]
    private Hole _holePrefab;

    [SerializeField]
    private float _radius;

    public Hole GetRandomHole()
    {
        var position = Random.insideUnitCircle * _radius;
        var hole = Instantiate(_holePrefab, transform);
        hole.transform.position = new Vector3(position.x, position.y) + transform.position;
        return hole;
    }

    private void OnDrawGizmosSelected()
    {
#if UNITY_EDITOR
        Handles.DrawWireDisc(transform.position, Vector3.forward, _radius);
#endif
    }
}