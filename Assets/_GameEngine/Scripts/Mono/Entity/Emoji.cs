using DG.Tweening;
using UnityEngine;

public class Emoji : MonoBehaviour
{
    private Sequence _animationMoving;
    private Sequence _animationScaling;

    public void Play()
    {
    }

    private void CreateMoving()
    {
        if (_animationMoving != null)
        {
            _animationMoving.Kill();
        }

        var up = transform.position + Vector3.up * Random.Range(0.1f, 0.3f);
        var down = transform.position;
        _animationMoving = DOTween.Sequence();
        _animationMoving.Append(transform.DOMove(up, 0.5f));
        _animationMoving.Append(transform.DOMove(down, 0.5f));
        _animationMoving.SetLoops(-1, LoopType.Yoyo);
    }

    private void CreateScaling()
    {
        if (_animationScaling != null)
        {
            _animationScaling.Kill();
        }

        _animationScaling = DOTween.Sequence();
        var localScaleX = transform.localScale.x * Random.Range(1.2f, 1.3f);
        _animationScaling.Append(transform.DOScale(localScaleX, 1f));
        _animationScaling.Append(transform.DOScale(transform.localScale.x * 0.8f, 1f));
        _animationScaling.SetDelay(Random.Range(0.1f, 0.7f));
        _animationScaling.SetLoops(-1, LoopType.Yoyo);
    }
}