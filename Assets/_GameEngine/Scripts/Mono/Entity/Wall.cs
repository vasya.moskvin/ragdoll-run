using UnityEngine;

public class Wall : MonoBehaviour
{
    [SerializeField]
    private DrillZone _drillZone;

    public DrillZone DrillZone => _drillZone;
}
