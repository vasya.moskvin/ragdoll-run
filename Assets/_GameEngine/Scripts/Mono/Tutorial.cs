using TMPro;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    [SerializeField]
    private GameObject _pressUp;

    [SerializeField]
    private GameObject _pressDown;

    [SerializeField]
    private TMP_Text _text;

    public GameObject PressUp => _pressUp;

    public GameObject PressDown => _pressDown;

    public TMP_Text Text => _text;
}