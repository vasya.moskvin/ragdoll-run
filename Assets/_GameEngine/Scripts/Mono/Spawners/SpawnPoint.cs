using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public T Spawn<T>(T prefab) where T : MonoBehaviour
    {
        var sliceObject = Instantiate(prefab, transform.position, transform.rotation);
        sliceObject.gameObject.SetActive(true);
        return sliceObject;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0, 1, 0, 0.3f);
        Gizmos.DrawSphere(transform.position, 1);
    }
#endif
}
