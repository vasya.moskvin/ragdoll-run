public interface IPodium
{
    void SelectLineByIndex(int index, MultiplySkin multiplySkin);
    void SetBaseColorLineByIndex(int index);
}
