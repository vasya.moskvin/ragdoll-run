using UnityEngine;

public class EnableCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var objectPart = other.GetComponent<ObjectPart>();
        if (objectPart != null)
        {
            other.isTrigger = false;
        }
    }
}