using System.Collections.Generic;
using UnityEngine;

public class WinEffects : MonoBehaviour
{
    [SerializeField]
    private List<ParticleSystem> _particleSystems;

    public void Awake()
    {
        foreach (var system in _particleSystems)
        {
            system.gameObject.SetActive(false);
        }
    }

    public void StartEffects()
    {
        foreach (var system in _particleSystems)
        {
            system.gameObject.SetActive(true);
            system.Play();
        }
    }
}
