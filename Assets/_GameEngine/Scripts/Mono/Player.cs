using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private GameObject _blot;

    [SerializeField]
    private Transform _visualModel;

    [SerializeField]
    private Rigidbody _rigidbody;

    [SerializeField]
    private float _offsetX;

    private float _targetX;
    public float Speed { get; private set; }
    public event Action<Boost> BoostAdded;

    private void Start()
    {
        Speed = 12;
        AddImpulse();
    }

    private void AddImpulse()
    {
        _rigidbody.velocity = GetImpulse(Speed);
    }

    private Vector3 GetImpulse(float value)
    {
        return (Vector3.up + Vector3.forward).normalized * value;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position + GetImpulse(100));
    }

    private void Update()
    {
        _rigidbody.transform.position = Vector3.Lerp(
            _rigidbody.transform.position,
            new Vector3(_targetX, _rigidbody.transform.position.y, _rigidbody.transform.position.z),
            Time.deltaTime);
    }

    private void LateUpdate()
    {
        _visualModel.position = transform.position;
    }

    public void MoveHorizontal(float horizontalDirection)
    {
        _targetX += horizontalDirection;
        _targetX = Mathf.Clamp(_targetX, -_offsetX, _offsetX);
    }

    public void ApplyBoost(Boost boost)
    {
        var boostSpeedEffect = boost.SpeedEffect;
        Speed += boostSpeedEffect;
        Speed = Mathf.Clamp(Speed, 0, 20);
        if (boost.SpeedEffect > 0)
        {
            _rigidbody.AddForce(GetImpulse(boostSpeedEffect), ForceMode.Impulse);
        }
        else
        {
            _rigidbody.AddForce(GetImpulse(boostSpeedEffect), ForceMode.Force);
        }

        BoostAdded?.Invoke(boost);
    }

    private void OnCollisionEnter(Collision other)
    {
        var ground = other.gameObject.GetComponent<Ground>();
        if (ground != null)
        {
            var blot = Instantiate(_blot);
            var transformPosition = other.contacts[0].point;
            transformPosition.y += 0.02f;
            blot.transform.position = transformPosition;
            Speed *= 0.95f;
            AddImpulse();
        }
    }

    private void SetKinematic(bool newValue)
    {
        var bodies = gameObject.GetComponentsInChildren<Rigidbody>();
        foreach (var body in bodies)
        {
            body.isKinematic = newValue;
        }
    }
}