using System;
using DG.Tweening;
using UnityEngine;

public class VacuumTube : MonoBehaviour
{
    [SerializeField]
    private Transform _target;

    [SerializeField]
    private GameObject _vacuum;

    private int _duration = 3;
    private bool _isSuccess;

    public event Action Completed;

    private void OnTriggerEnter(Collider other)
    {
        var objectPart = other.GetComponent<ObjectPart>();
        if (objectPart != null)
        {
            _isSuccess = true;
            objectPart.transform.DOMove(_target.position, _duration);

            if (_isSuccess)
            {
                Completed?.Invoke();
            }
        }
    }
}