using UnityEngine;

public class YRotator : MonoBehaviour
{
#if UNITY_EDITOR
    private int _rotationSpeed = 90;
#endif

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.A))
        {
            Vector3 v = new Vector3(0, transform.localRotation.y + _rotationSpeed * Time.deltaTime, 0);
            transform.localRotation = Quaternion.Euler(v);
            return;
        }

        if (Input.GetKey(KeyCode.D))
        {
            Vector3 v = new Vector3(0, transform.localRotation.y - _rotationSpeed * Time.deltaTime, 0);
            transform.localRotation = Quaternion.Euler(v);
            return;
        }
#endif
        if (Input.touchCount > 0)
        {
            var firstFinger = Input.GetTouch(0);

            if (firstFinger.phase == TouchPhase.Moved)
            {
                var speed = firstFinger.deltaPosition.x / Screen.width;
                transform.Rotate(0, -360 * speed, 0);
            }
        }
    }
}
