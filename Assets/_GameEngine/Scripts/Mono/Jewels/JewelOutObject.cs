using System;
using UnityEngine;

public class JewelOutObject : MonoBehaviour
{
    [SerializeField]
    private SpawnPoint _spawnPoint;

    [SerializeField]
    private JewelObject _jewel;

    private void OnTriggerEnter(Collider other)
    {
        var jewel = other.GetComponent<JewelObject>();
        if (jewel != null)
        {
            Destroy(jewel.gameObject);
            _spawnPoint.Spawn(_jewel);
        }
    }
}
