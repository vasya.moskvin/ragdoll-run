using UnityEngine;

public class JewelObject : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _rigidbody;

    [SerializeField]
    private Collider _collider;

    public float TimeOpening;
    public float DurationOpen = 2;
    public float ProgressOpening => TimeOpening / DurationOpen;
    public bool IsOpening { get; set; }

    public float GetTime()
    {
        return TimeOpening;
    }

    public void SetTime(float value)
    {
        TimeOpening = value;
    }

    public void EnableCollider()
    {
        _collider.isTrigger = false;
    }

    public void Take(Transform parent)
    {
        _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        _rigidbody.useGravity = false;
        _collider.isTrigger = true;
        transform.parent = parent;
        transform.localPosition = Vector3.zero;
    }

    public void Drop()
    {
        _rigidbody.constraints = RigidbodyConstraints.None;
        _rigidbody.useGravity = true;
        _collider.isTrigger = false;
        transform.parent = null;
        TimeOpening = 0;
    }
}