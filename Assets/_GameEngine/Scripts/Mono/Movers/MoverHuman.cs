using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MoverHuman : Mover
{
    [SerializeField]
    private Animator _animator;

    private Sequence _sequence;

    private static readonly int MovementX = Animator.StringToHash("MovementX");
    private static readonly int IsDance = Animator.StringToHash("IsDance");

    public override void MoveTo(List<Vector3> points, float speed, Action callback)
    {
        if (_sequence != null)
        {
            _sequence.Kill();
        }

        _sequence = DOTween.Sequence();
        points.Insert(0, transform.position);

        for (var i = 1; i < points.Count; i++)
        {
            var point = points[i];
            var d = Vector3.Distance(points[i - 1], points[i]);

            var movePoint = transform.DOMove(point, d / speed)
                                     .SetEase(Ease.Linear)
                                     .OnPlay(
                                         () =>
                                         {
                                             _animator.SetFloat(MovementX, 1);
                                             _animator.applyRootMotion = false;
                                         });
            var lookAt = transform.DOLookAt(point, 1f).SetEase(Ease.Linear).OnPlay(
                () =>
                {
                    _animator.SetFloat(MovementX, 0);
                });
            _sequence.Append(lookAt);
            _sequence.Append(movePoint);
        }

        _sequence.OnComplete(
            () =>
            {
                callback?.Invoke();
            });
        // var randomPositionForDance = Random.insideUnitSphere * 1.5f;
        // randomPositionForDance.y = 0;
        // var moveDancePoint = transform.DOMove(randomPositionForDance, 0.5f)
        //                               .SetEase(Ease.Linear)
        //                               .OnPlay(() => transform.LookAt(randomPositionForDance))
        //                               .OnStepComplete(() =>
        //                               {
        //                                   _animator.SetFloat(MovementX, 0);
        //                                   _animator.SetTrigger(IsDance);
        //                                   _animator.applyRootMotion = true;
        //                               });
        //
        // _sequence.Append(moveDancePoint);
    }

    public override void Stop()
    {
        if (_sequence != null)
        {
            _sequence.Kill();
        }
        _animator.SetFloat(MovementX, 0);
    }
}

public class MoverPatrol: Mover
{
    
}
