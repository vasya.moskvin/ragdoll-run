using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public virtual void MoveTo(List<Vector3> points, float speed, Action callback)
    {
        var sequence = DOTween.Sequence();
        points.Insert(0, transform.position);
        for (var i = 1; i < points.Count; i++)
        {
            var point = points[i];
            var d = Vector3.Distance(points[i - 1], points[i]);
            sequence.Append(transform.DOMove(point, d / speed).SetEase(Ease.Linear));
        }
    }

    public virtual void Stop()
    {
    }
}