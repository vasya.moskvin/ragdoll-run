using System;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Player _target;

    [SerializeField]
    private float _offsetZ;

    private float _minY;
    private void Start()
    {
        _minY = transform.position.y - 5;
    }

    [ContextMenu("Set Offset Position")]
    private void SetNewPosition()
    {
        var position = transform.position;
        var positionY = Mathf.Max(_target.transform.position.y, _minY) + 5;
        position = new Vector3(position.x, positionY, _target.transform.position.z + _offsetZ);
        transform.position = position;
    }

    private void LateUpdate()
    {
        SetNewPosition();
    }
}
