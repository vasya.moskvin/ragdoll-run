using System;
using System.Collections.Generic;
using UnityEngine;

public class BoostGenerator : MonoBehaviour
{
    [SerializeField]
    private List<Boost> _boosts;

    [SerializeField]
    private List<BoostLine> _boostGates;

    [SerializeField]
    private float _startZ;

    [SerializeField]
    private float _finishZ;

    [SerializeField]
    private float _minStep;

    [SerializeField]
    private float _maxStep;

    [SerializeField, HideInInspector]
    private List<BoostLine> _list = new List<BoostLine>();

    private readonly Vector3 _basePosition = new Vector3(0, 0.01f, 0);

    private void Start()
    {
        Generate();
    }

    public void Generate()
    {
        foreach (var boostGate in _list)
        {
            DestroyImmediate(boostGate.gameObject);
        }
        _list.Clear();
        for (var i = _startZ; i < _finishZ; i += UnityEngine.Random.Range(_minStep, _maxStep))
        {
            var boostGate = Instantiate(_boostGates.RandomItem(), transform);
            boostGate.transform.position = new Vector3(_basePosition.x, _basePosition.y, i);
            var selectedBoost = new List<Boost>();
            for (var j = 0; j < boostGate.CountBoost; j++)
            {
                selectedBoost.Add(_boosts.RandomItem());
            }

            boostGate.Initialization(selectedBoost);
            _list.Add(boostGate);
        }
    }
}