using TMPro;
using UnityEngine;

public class BoostGate : MonoBehaviour
{
    [SerializeField]
    private Renderer _renderer;

    [SerializeField]
    private TMP_Text _bonus;

    private Boost _boost;

    private const string PositiveFormat = "+{0}";
    private const string NegativeFormat = "{0}";

    public void Initialization(Boost boost)
    {
        _renderer.material = boost.Material;
        _boost = boost;
        _bonus.text = string.Format(
            boost.SpeedEffect > 0
                ? PositiveFormat
                : NegativeFormat, boost.SpeedEffect);
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<Player>();
        if (player != null &&
            _boost != null)
        {
            player.ApplyBoost(_boost);
        }
    }
}
