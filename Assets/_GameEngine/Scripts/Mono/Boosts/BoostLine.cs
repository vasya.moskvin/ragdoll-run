using System.Collections.Generic;
using UnityEngine;

public class BoostLine : MonoBehaviour
{
    [SerializeField]
    private List<BoostGate> _boostGates = new List<BoostGate>();

    public int CountBoost => _boostGates.Count;

    public void Initialization(List<Boost> boosts)
    {
        for (var i = 0; i < _boostGates.Count; i++)
        {
            _boostGates[i].Initialization(boosts[i % boosts.Count]);
        }
    }
}