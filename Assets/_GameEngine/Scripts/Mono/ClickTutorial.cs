using DG.Tweening;
using UnityEngine;

public class ClickTutorial : MonoBehaviour
{
    [SerializeField]
    private GameObject _hand;

    [SerializeField]
    private GameObject _startPoint;

    private Sequence _sequence;

    private void OnEnable()
    {
        if (_sequence != null)
        {
            _sequence.Kill();
        }

        _hand.transform.position = _startPoint.transform.position;
        var tapDown = _hand.transform.DORotate(new Vector3(45, 0, 0), 0.5f).SetEase(Ease.Linear);
        var tapUp = _hand.transform.DORotate(new Vector3(0, 0, 0), 0.5f).SetEase(Ease.Linear);
        _sequence = DOTween.Sequence();
        _sequence.Append(tapDown);
        _sequence.Append(tapUp);
        _sequence.SetLoops(-1, LoopType.Restart);
    }
}
