using System.Collections.Generic;
using UnityEngine;

public class StripesGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject _strip;

    [SerializeField]
    private float _startZ;

    [SerializeField]
    private float _finishZ;

    [SerializeField]
    private float _step;

    [SerializeField, HideInInspector]
    private List<GameObject> _list = new List<GameObject>();

    private readonly Vector3 _basePosition = new Vector3(0, 0.01f, 0);

    [ContextMenu("Generate")]
    public void Generate()
    {
        foreach (var strip in _list)
        {
            DestroyImmediate(strip);
        }
        _list.Clear();
        for (var i = _startZ; i < _finishZ; i += _step)
        {
            var strip = Instantiate(_strip, transform);
            strip.transform.position = new Vector3(_basePosition.x, _basePosition.y, i);
            _list.Add(strip);
        }
    }
}