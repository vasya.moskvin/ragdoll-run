using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScenes : MonoBehaviour
{
    [SerializeField]
    private List<string> _sceneEnvironmentName = new List<string>();

    private int _index;
    private int _countLevelForChange = 1;
    private string _currentScene;

    public event Action SceneLoaded;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public bool TryLoadLevel(string newScene)
    {
        // if (newScene.Equals(_currentScene))
        // {
        //     return false;
        // }

        if (!string.IsNullOrEmpty(_currentScene))
        {
            SceneManager.UnloadSceneAsync(_currentScene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        }

        _currentScene = newScene;
        StartCoroutine(LoadScene(_currentScene));
        return true;
    }

    public bool TryLoadLevelByIndex(int index)
    {
        if (_sceneEnvironmentName.Count == 0)
        {
            return false;
        }

        index /= _countLevelForChange;
        var newScene = _sceneEnvironmentName[index % _sceneEnvironmentName.Count];
        return TryLoadLevel(newScene);
    }

    private IEnumerator LoadScene(string sceneName)
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        yield return null;

        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        SceneLoaded?.Invoke();
    }

    private IEnumerator EnvironmentLoop()
    {
        while (true)
        {
            SceneManager.LoadScene(_sceneEnvironmentName[_index], LoadSceneMode.Additive);
            yield return null;
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(_sceneEnvironmentName[_index]));
            yield return new WaitForSeconds(5f);
            SceneManager.UnloadSceneAsync(_sceneEnvironmentName[_index], UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            _index = (_index + 1) % _sceneEnvironmentName.Count;
        }
    }
}