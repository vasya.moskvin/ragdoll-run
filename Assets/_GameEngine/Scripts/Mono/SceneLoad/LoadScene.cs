using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour
{
    [SerializeField]
    private string _sceneName;

    [SerializeField]
    private Button _loadScene;

    private void Start()
    {
        _loadScene.onClick.AddListener(
            () =>
            {
                SceneManager.LoadScene(_sceneName, LoadSceneMode.Single);
            });
    }
}
