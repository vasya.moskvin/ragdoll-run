using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChanger : MonoBehaviour
{
    [SerializeField]
    private GameObject _fpsCamera;

    [SerializeField]
    private GameObject _tpsCamera;

    public void SetFps()
    {
        _fpsCamera.SetActive(false);
        _tpsCamera.SetActive(true);
    }

    public void SetTps()
    {
        _fpsCamera.SetActive(true);
        _tpsCamera.SetActive(false);
    }
}
