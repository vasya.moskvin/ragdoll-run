public class GlobalSettings
{
    public static VibrationSettings NoneVibration = new VibrationSettings { Duration = 0, Amplitude = 0 };
    public static VibrationSettings ChangeVibration = new VibrationSettings { Duration = 200, Amplitude = 100 };
    public static VibrationSettings SaveVibration = new VibrationSettings { Duration = 100, Amplitude = 100 };
    public static VibrationSettings DieVibration = new VibrationSettings { Duration = 500, Amplitude = 100 };
}
