using MoreMountains.NiceVibrations;

public class VibrationMediator
{
    public static void Vibrate(VibrationSettings vibrationSettings)
    {
        if (!AccountData.GetAccountModel().IsEnableVibration)
        {
            return;
        }

        try
        {
            MMNVAndroid.AndroidVibrate(vibrationSettings.Duration, vibrationSettings.Amplitude);
        }
        catch
        {
            // ignored
        }
    }
}
