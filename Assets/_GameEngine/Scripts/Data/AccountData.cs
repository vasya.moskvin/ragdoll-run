using UnityEngine;

public class AccountData
{
    private static AccountModel accountModel;
    private const string KeyAccountModel = "Snx9n3WZQn3sh5Zf";

    public static AccountModel GetAccountModel()
    {
        if (accountModel != null)
        {
            return accountModel;
        }

        accountModel = Load() ?? new AccountModel();

        return accountModel;
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Data/Remove Account")]
    public static void RemoveAccount()
    {
        PlayerPrefs.DeleteKey(KeyAccountModel);
        PlayerPrefs.Save();
        Debug.Log("Success remove account data");
    }
#endif

    public static void Save()
    {
        var json = JsonUtility.ToJson(GetAccountModel());
        PlayerPrefs.SetString(KeyAccountModel, json);
        PlayerPrefs.Save();
    }

    private static AccountModel Load()
    {
        var json = PlayerPrefs.GetString(KeyAccountModel);
        return JsonUtility.FromJson<AccountModel>(json);
    }
}