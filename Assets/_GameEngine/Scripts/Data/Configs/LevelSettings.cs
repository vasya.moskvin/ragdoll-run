using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameData/Create Level Settings", fileName = "Level", order = 0)]
public class LevelSettings : ScriptableObject
{
    [SerializeField]
    private string _id = "need@change";

    [SerializeField]
    private string _sceneName = "need@change";

    [SerializeField]
    private bool _isTutorial = default;

    [SerializeField]
    private List<BitSettings> _bitSettings = default;

    [SerializeField, Range(0,1)]
    private float _boundStartNoisePercent;

    [SerializeField, Range(0,1)]
    private float _speedDecreaseNoisePercent;

    [SerializeField, Range(0,1)]
    private float _speedIncreaseNoisePercent;

    [SerializeField]
    private float _difficultyDrillingHole;

    [SerializeField]
    private AudioClip _drillAudioClip;

    [SerializeField]
    private Material _drillMaterialFirst;

    [SerializeField]
    private Material _drillMaterialSecond;

    [SerializeField]
    private Wall _wall;

    [SerializeField]
    private List<TutorialStep> _tutorialSteps;

    public string Id => _id;
    public string SceneName => _sceneName;
    public bool IsTutorial => _isTutorial;
    public List<TutorialStep> TutorialSteps => _tutorialSteps;
    public List<BitSettings> BitSettings => _bitSettings;

    public float BoundStartNoisePercent => _boundStartNoisePercent;

    public float SpeedDecreaseNoisePercent => _speedDecreaseNoisePercent;

    public float SpeedIncreaseNoisePercent => _speedIncreaseNoisePercent;

    public float DifficultyDrillingHole => _difficultyDrillingHole;

    public AudioClip DrillAudioClip => _drillAudioClip;

    public Material DrillMaterialFirst => _drillMaterialFirst;

    public Material DrillMaterialSecond => _drillMaterialSecond;

    public Wall Wall => _wall;
}