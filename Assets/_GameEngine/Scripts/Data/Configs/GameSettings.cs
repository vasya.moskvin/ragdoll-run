using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameData/Create Game Settings", fileName = "GameSettings", order = 0)]
public class GameSettings : ScriptableObject
{
    [SerializeField]
    private List<LevelSettings> _levels = new List<LevelSettings>();

    [SerializeField]
    private string _linkGooglePlay = "https://www.google.com/";

    [SerializeField]
    private string _linkAppleStore = "https://www.apple.com/";

    public List<LevelSettings> Levels => _levels;
    public string LinkGooglePlay => _linkGooglePlay;
    public string LinkAppleStore => _linkAppleStore;
}