using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "GameData/Create UnitSkinStorage", fileName = "UnitSkinStorage", order = 0)]
public class UnitSkinStorage : ScriptableObject
{
    [SerializeField]
    private UnitSkinStorageElement _default;

    [SerializeField]
    private List<UnitSkinStorageElement> _storageElements = new List<UnitSkinStorageElement>();

    public UnitSkinStorageElement GetSkinByType(SkinType type)
    {
        var element = _storageElements.FirstOrDefault(skin => skin.Type == type);
        return element ?? _default;
    }
}