using System;
using UnityEngine;

[Serializable]
public class UnitSkinStorageElement
{
    [SerializeField]
    private SkinType _type;

    [SerializeField]
    private Unit _enemy;

    [SerializeField]
    private Unit _player;

    public SkinType Type => _type;

    public Unit Enemy => _enemy;

    public Unit Player => _player;
}
