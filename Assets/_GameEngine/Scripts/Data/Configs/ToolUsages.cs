using System;
using UnityEngine;

[Serializable]
public class ToolUsages
{
    [SerializeField]
    private ToolType _type;

    [SerializeField]
    private int _count;

    public ToolType Type => _type;

    public int Count => _count;
}