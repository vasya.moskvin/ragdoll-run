using UnityEngine;

[CreateAssetMenu(menuName = "GameData/Create Bit Settings", fileName = "Bit", order = 0)]
public class BitSettings : ScriptableObject
{
    [SerializeField]
    private Bit _body;

    [SerializeField]
    private string _id = "need@change";

    public Bit Body => _body;

    public string Id => _id;
}
