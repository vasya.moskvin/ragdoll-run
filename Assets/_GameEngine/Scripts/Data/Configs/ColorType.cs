public enum ColorType
{
    None = 0,
    Red = 1,
    Blue = 2,
    Orange = 3,
    Green = 4,
    Purple = 5
}
