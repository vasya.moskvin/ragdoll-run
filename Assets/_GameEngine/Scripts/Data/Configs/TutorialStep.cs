using System;
using UnityEngine;

[Serializable]
public class TutorialStep
{
    [SerializeField]
    private float _timeScale = 1;

    [SerializeField]
    private string _text;

    [SerializeField]
    private bool _isHandUnit;

    // public bool IsHandBoatButton => _isHandBoatButton;
    //
    // public bool IsHandLifeBuoyButton => _isHandLifeBuoyButton;

    public bool IsHandUnit => _isHandUnit;
}

public enum TutorialState
{
    DrillOne,
    BreakingDrill,
    DrillTwo
}
