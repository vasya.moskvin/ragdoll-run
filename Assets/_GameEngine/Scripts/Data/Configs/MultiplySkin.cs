using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameData/Create Multiply Skin", fileName = "MultiplySkin", order = 0)]
public class MultiplySkin : ScriptableObject
{
    [SerializeField]
    private ColorType _colorType;
    [SerializeField]
    private Color _color;
    [SerializeField]
    private Material _materialRing;
    [SerializeField]
    private Material _materialUnit;
    [SerializeField]
    private Sprite _sprite;
    [SerializeField]
    private List<GameObject> _destroyEffects;

    public Color Color => _color;
    public Material MaterialRing => _materialRing;
    public Material MaterialUnit => _materialUnit;
    public ColorType ColorType => _colorType;
    public Sprite Sprite => _sprite;
    public GameObject DestroyEffect => GetRandomDestroyEffect();

    private GameObject GetRandomDestroyEffect()
    {
        var index = UnityEngine.Random.Range(0, _destroyEffects.Count);
        return _destroyEffects[index];
    }
}