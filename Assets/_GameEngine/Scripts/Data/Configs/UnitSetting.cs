using System;
using UnityEngine;

[Serializable]
public class UnitSetting
{
    [SerializeField]
    private Unit _unit = default;

    [SerializeField]
    private float _maxTime = default;

    [SerializeField]
    private float _startTime = default;

    public Unit Unit => _unit;

    public float MaxTime => _maxTime;

    public float StartTime => _startTime;
}
