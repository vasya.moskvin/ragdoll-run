using System;
using System.Linq;
using UnityEngine;

public class LevelModel
{
    private readonly LevelSettings _levelSettings;
    private readonly int _levelNumber;
    public LevelSettings LevelSettings => _levelSettings;
    public int LevelNumber => _levelNumber;
    public event Action NoisePeaked;
    public event Action ForcePeaked;
    public event Action NoiseEmpty;
    public event Action NoiseChanged;
    public event Action ForceChanged;
    public event Action BitChanged;
    public event Action GoalFinished;
    public event Action DrillButtonDown;
    public event Action DrillButtonUp;
    public event Action<HoleModel> DrillStarted;

    public BitSettings BitSettings { get; private set; }
    public float Noise { get; set; }
    public float MaxNoise { get; set; }
    public float Force { get; set; }
    public float MaxForce { get; set; }
    public HoleModel CurrentHoleModel => _currentHoleModel;

    private bool _isDrilling;
    private HoleModel _currentHoleModel;
    private bool _isPressingButtonDrill;
    private bool _isAvailableDecreaseNoise;
    public LevelModel(
        int levelNumber,
        LevelSettings levelSettings)
    {
        _levelSettings = levelSettings;
        _levelNumber = levelNumber;
        BitSettings = levelSettings.BitSettings.FirstOrDefault();
    }

    public void Drill(float deltaTime)
    {
        AddNoise(MaxNoise * _levelSettings.SpeedIncreaseNoisePercent * deltaTime);
        AddForce(deltaTime);
        if (!_isPressingButtonDrill && _isDrilling)
        {
            _isPressingButtonDrill = true;
            DrillButtonDown?.Invoke();
        }
    }

    public void NotDrill(float deltaTime)
    {
        if (_isAvailableDecreaseNoise)
        {
            AddNoise(-MaxNoise * _levelSettings.SpeedDecreaseNoisePercent * deltaTime);
        }
        AddForce(-deltaTime);
        if (_isPressingButtonDrill)
        {
            _isPressingButtonDrill = false;
            DrillButtonUp?.Invoke();
        }
    }

    public float GetNoiseProgress()
    {
        return Noise / MaxNoise;
    }

    private void AddForce(float delta)
    {
        if (!_isDrilling)
        {
            return;
        }

        if (Force >= MaxForce)
        {
            ForcePeaked?.Invoke();
            _isDrilling = false;
        }

        Force += delta;
        Force = Mathf.Clamp(Force, 0, MaxForce);
        _currentHoleModel?.AddProgress(Force);
        ForceChanged?.Invoke();
    }

    private void AddNoise(float delta)
    {
        if (!_isDrilling)
        {
            return;
        }

        Noise += delta;

        if (Noise <= 0)
        {
            NoiseEmpty?.Invoke();
            _isDrilling = false;
        }

        if (Noise >= MaxNoise)
        {
            NoisePeaked?.Invoke();
            _isDrilling = false;
        }

        if (!_isAvailableDecreaseNoise)
        {
            var percentNoise = Noise / MaxNoise;
            if (percentNoise >= _levelSettings.BoundStartNoisePercent)
            {
                _isAvailableDecreaseNoise = true;
            }
        }

        Noise = Mathf.Clamp(Noise, 0, MaxNoise);
        NoiseChanged?.Invoke();
    }

    public void SetBit(BitSettings setting)
    {
        if (_isDrilling)
        {
            return;
        }

        BitSettings = setting;
        BitChanged?.Invoke();
    }

    public void ReloadDrill()
    {
        _isDrilling = true;
    }

    public void StartDrill()
    {
        _isDrilling = true;
        Force = 0;
        _currentHoleModel = new HoleModel(_levelSettings.DifficultyDrillingHole, FinishDrill);
        DrillStarted?.Invoke(_currentHoleModel);
    }

    public void FinishDrill()
    {
        _isDrilling = true;
        GoalFinished?.Invoke();
        StartDrill();
    }
}

public class TutorialModel
{
    public event Action<TutorialModel> Changed;

    public bool IsNeedPressUp { get; private set; }
    public bool IsNeedPressDown { get; private set; }
    public string TutorialText { get; private set; }

    public void SetText(string text)
    {
        TutorialText = text;
        Changed?.Invoke(this);
    }

    public void SetNeedPressUp(bool isActive)
    {
        IsNeedPressUp = isActive;
        Changed?.Invoke(this);
    }

    public void SetNeedPressDown(bool isActive)
    {
        IsNeedPressDown = isActive;
        Changed?.Invoke(this);
    }

}

public enum ToolType
{
    Lifebuoy,
    Boat,
    Rope,
}