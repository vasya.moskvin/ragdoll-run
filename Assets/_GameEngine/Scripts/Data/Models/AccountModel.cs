using System;
using UnityEngine;

[Serializable]
public class AccountModel
{
    [SerializeField]
    private int _countFinishLevel;

    [SerializeField]
    private int _gems;

    [SerializeField]
    private int _coins;

    [SerializeField]
    private string _nextIdLevel;

    [SerializeField]
    private bool _isEnableVibration = true;

    public int Gems => _gems;

    public int Coins => _coins;

    public int NextLevelIndex => _countFinishLevel;
    public int NextLevelNumber => _countFinishLevel + 1;

    public string NextIdLevel
    {
        get => _nextIdLevel;
        set => _nextIdLevel = value;
    }

    public bool IsEnableVibration
    {
        get => _isEnableVibration;
        set => _isEnableVibration = value;
    }

    public void AddGems(int count)
    {
        if (count <= 0)
        {
            return;
        }

        _gems += count;
    }

    /// <summary>
    /// Remove gems
    /// </summary>
    /// <param name="count"></param>
    /// <returns>true is success removing</returns>
    public bool TryRemoveGems(int count)
    {
        if (count < 0 || count > _gems)
        {
            return false;
        }

        _gems -= count;
        return true;
    }

    public void AddCoins(int count)
    {
        if (count <= 0)
        {
            return;
        }

        _coins += count;
    }

    /// <summary>
    /// Remove coins
    /// </summary>
    /// <param name="count"></param>
    /// <returns>true is success removing</returns>
    public bool TryRemoveCoins(int count)
    {
        if (count < 0 || count > _coins)
        {
            return false;
        }

        _coins -= count;
        return true;
    }

    public void FinishLevel()
    {
        _countFinishLevel++;
    }
}