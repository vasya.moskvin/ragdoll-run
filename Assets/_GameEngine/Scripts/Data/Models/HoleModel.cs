using System;
using UnityEngine;

public class HoleModel
{
    private readonly float _maxForce;
    private readonly Action _success;
    private float _currentForce;
    private bool _isSuccess;

    public HoleModel(float maxForce, Action success)
    {
        _maxForce = maxForce;
        _success = success;
    }

    public float GetProgress()
    {
        return Mathf.Clamp01(_currentForce / _maxForce);
    }

    public void AddProgress(float delta)
    {
        if (delta <= 0 || _isSuccess)
        {
            return;
        }

        _currentForce += delta;
        if (_currentForce >= _maxForce)
        {
            _isSuccess = true;
            _success?.Invoke();
        }
    }
}
